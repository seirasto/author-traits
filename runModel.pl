#!/usr/local/bin/perl

# acceptable actions: 
# a. run: run the model 
# b. model: generate the model

$type = $ARGV[0];
$action = $ARGV[1];
$input = $ARGV[2];
$output = $ARGV[3];

$typeu = ucfirst($type);
print "Type: $type\n";

$cmd = "/usr/lib/jvm/java-1.7.0-openjdk-amd64/bin/java -Dfile.encoding=UTF-8 -classpath '/proj/fluke/users/sara/java/Author_Traits/jars/*:' edu.columbia.demographics.$type." . $typeu . "Detector -$action $input $output";
print "running model...\n";
print $cmd;
`$cmd`;

