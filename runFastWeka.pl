#!/usr/local/bin/perl

$action = $ARGV[0]; # run (generate are file), filter (filter arff file)

if ($action eq "run") {
$type = $ARGV[1]; # age, religion, year, gender, politics
$source = $ARGV[2]; # lj, twitter, authorship
$start = $ARGV[3]; # position to start at
$nb = $ARGV[4]; # number of bloggers
$output = $ARGV[5]; # output directory

    $cmd = "/usr/lib/jvm/java-1.7.0-openjdk-amd64/bin/java -Dfile.encoding=UTF-8 -classpath '/proj/fluke/users/sara/java/age/jars/*:' edu.columbia.demographics.FastWeka -action run -r $type -nb $nb -s $start -nf 1000 -pos -ngrams -collocations -sm -liwc -o $output -source $source";
    print "Generating arff file...\n";
    print "$cmd\n";
    `$cmd`;
}

# to run this filter the entire arff file must be ready. If the arff file was generated in multiple steps run this after it is concatenated. Pass along the appropriate files
if ($action eq "filter") {
    $cmd = "/usr/lib/jvm/java-1.7.0-openjdk-amd64/bin/java -Dfile.encoding=UTF-8 -classpath '/proj/fluke/users/sara/java/age/jars/*:' edu.columbia.demographics.FastWeka -action filter -r $ARGV[1] -trainArff $ARGV[2] -testArff $ARGV[3]";
    print "Filtering...\n";
    print "$cmd\n";
    `$cmd`;
}
