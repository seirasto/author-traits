#!/usr/local/bin/perl

$type = $ARGV[0];
$nb = $ARGV[1];
$output = $ARGV[2];

print "Type: $type";

$cmd = "/usr/lib/jvm/java-1.7.0-openjdk-amd64/bin/java -Dfile.encoding=UTF-8 -classpath '/proj/fluke/users/sara/java/age/jars/*:' edu.columbia.demographics.Processing -action xtract -type $type -nb $nb -output $output";
print "Generating xtract files...\n";
print "$cmd\n";
`$cmd`;

