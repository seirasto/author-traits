/**
 * 
 */
package edu.columbia.demographics;

import blog.threaded.livejournal.*;
import edu.columbia.demographics.gender.Label;
import edu.stanford.nlp.parser.lexparser.*;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
//import edu.stanford.nlp.trees.*;
import collocation.*;

import java.util.*;
import java.io.*;
import java.text.SimpleDateFormat;

import processing.GeneralUtils;
import processing.SwapEmoticons;

/**
 * @author sara
 *
 */
public class Processing {
	
	String _tagger;
	String _parser;
	
	public Processing() {
   		Properties prop = GeneralUtils.getProperties("config/config.properties");
   		_tagger = prop.getProperty("tagger");
   		_parser = prop.getProperty("parser");
	}
	
	class HashClass implements Comparable<HashClass> {
		
		String key;
		Integer value;
		
		public HashClass(String keyIn, int valueIn) {
			key = keyIn;
			value = valueIn;
		}
		
		public String getKey() {
			return key;
		}
		
		public Integer getValue() {
			return value;
		}
		
		public int compareTo(HashClass in) {
			return in.getValue().compareTo(value);
		}
		
		public String toString() {
			return key + ", " + value;
		}
	}
	
	private List<HashClass> convertHash(Hashtable<String,Integer> data) {
		List<HashClass> listData = new ArrayList<HashClass>();
		
		if (data == null) return listData;
		Iterator<String> it = data.keySet().iterator();
		
		while (it.hasNext()) {
			String key = (String)it.next();
			int value = data.get(key);
			listData.add(new HashClass(key,value));
		}
		
		return listData;
	}
	
    public void addPOSandDependencies(String list, String inputDirectory, String outputDirectory, int start, int count) throws Exception {
    	//LexicalizedParser lexicalizedParser = LexicalizedParser.loadModel(PARSER);
    	LexicalizedParser lexicalizedParser = LexicalizedParser.loadModel();
   		MaxentTagger tagger = new MaxentTagger(_tagger);
    	
    	boolean updateComments = false; // to go faster
        
        System.err.println("[Processing.addPOSandDependencies] processing " + count + " bloggers");
                               
        List<String> bloggers = new ArrayList<String>();
        
        try {
    		// load emoticon file for swapping
    		SwapEmoticons swapper = new SwapEmoticons();
    		
    		// read directory
    		if (list == null) {
    			bloggers = Arrays.asList(new File(inputDirectory).list());
    		}
    		else {
	        	BufferedReader in = new BufferedReader(new FileReader(list));
	
	            String inputLine;
	            while ((inputLine = in.readLine()) != null) {
	            	bloggers.add(inputLine);
	            }
	            in.close();
    		}
    		
            int i = 0; // keep track of number of files processed successfully
                
	        for (int index = 0; index < bloggers.size() && i < count; index++) {
                if (index < start) continue;
                if (new File(outputDirectory + "/" + bloggers.get(index)).exists()) {
            		i++;
            		continue;                                
            	}
                System.err.println("[Processing.addPOSandDependencies] " + i + 
                		". adding Dependencies & POS to " + bloggers.get(index));
            	
                try {
                	//Blog.addSyntaxDependencies(inputDirectory, outputDirectory,
                	//		bloggers.get(index).replace(".xml", ""), tagger, lexicalizedParser, updateComments, swapper);
                	Blog.addSyntaxDependencies(inputDirectory + "/" + bloggers.get(index), outputDirectory + "/" + bloggers.get(index), tagger, lexicalizedParser, 
                			updateComments, swapper);
                } catch (Exception e) {
                    System.err.println("[Processing.addPOSandDependencies] adding syntax dependencies: " + e);
                    e.printStackTrace();
                    continue;
                }
                i++;
	        }
	        System.err.println("[Processing.addPOSandDependencies] completed adding syntax dependencies");
        } catch (Exception e) {
        	System.err.println("[Processing.addPOSandDependencies] " + e);
            e.printStackTrace();
        }
    }
    
    public void runXtractOnAgeGroups(int min, int max, List<String> ageLists, List<String> profilesDirectories, 
    	List<String> entriesDirectories, String outputDirectory, int numBlogs, int numWords, String comparer, int amount) {
    	
    	List<String> group  = new ArrayList<String>();
    	
    	for (String ageList : ageLists) {
	    	try {
	        	BufferedReader in = new BufferedReader(new FileReader(ageList));
	 
	            String inputLine;
	            while ((inputLine = in.readLine()) != null) {
	            	String [] data = inputLine.split(",");
	            	int age = Integer.valueOf(data[2]);
	            	if (age >= min && age  <= max) group.add(data[0]);
	            }
	            in.close();
	    	} catch (Exception e) {
	    		System.err.println("[Processing.runXtract] " + e);
	    		e.printStackTrace();
	    	}
    	}
    	System.err.println("[Processing.runXtractOnAgeGroups] Size (" + min + "-" + max + "): " + group.size());
    	
    	// use a random set of bloggers
    	Collections.shuffle(group);
    	group = group.subList(0, numBlogs);

    	System.err.println("[Processing.runXtractOnAgeGroups] Running Xtract on age group " + min + "-" + max);
    	Xtract xtract = new Xtract(_parser, outputDirectory + "/" + min + "-" + max + "/", "csubj|nn|dobj|amod",1,1,10,.75);
    	Hashtable<String,Integer> interests = new Hashtable<String,Integer>();
    	for (int i = 0; i < entriesDirectories.size(); i++) {
	    		runXtractP1(xtract, group, profilesDirectories.get(i), entriesDirectories.get(i), outputDirectory + "/" + min + "-" + max + "/", numBlogs, numWords, comparer, interests, false);	
	    }
    	this.runXtractP2(xtract, outputDirectory, comparer, amount, interests);	
    }
    
    public void runXtractOnGender(List<String> genderLists, List<String> profilesDirectories, 
        	List<String> entriesDirectories, String outputDirectory, int numBlogs, int numWords, String comparer, int amount) {
        	
        	List<Hashtable<String, String>> blogGender = new ArrayList<Hashtable<String, String>>();

        	for (int i = 0; i < genderLists.size(); i++) {
        		blogGender.add(new Hashtable<String, String>());
	        	try {
	            	BufferedReader in = new BufferedReader(new FileReader(genderLists.get(i)));
	     
	                String inputLine;
	                while ((inputLine = in.readLine()) != null) {
	                	String [] data = inputLine.split(",");
	                	blogGender.get(i).put(data[0],data[1]);
	                }
	                in.close();
	        	} catch (Exception e) {
	        		System.err.println("[Processing.runXtract] " + e);
	        		e.printStackTrace();
	        	}
    		}
    		List<ArrayList<String>> male = new ArrayList<ArrayList<String>>();
    		List<ArrayList<String>> female = new ArrayList<ArrayList<String>>();
    		
    		for (int i = 0; i < blogGender.size(); i++) {
    			male.add(new ArrayList<String>());
    			female.add(new ArrayList<String>());
	     		Iterator<String> it = blogGender.get(i).keySet().iterator();
	    		
	    		while (it.hasNext()) {
	    			String blogger = (String)it.next();
	    			
	    			if (blogGender.get(i).get(blogger).equals("M")) male.get(i).add(blogger);
	    			else if (blogGender.get(i).get(blogger).equals("F")) female.get(i).add(blogger);
	    		}
	    		// use a random set of bloggers
	    		if (numBlogs > 0 && numBlogs < male.size()) {
		        	Collections.shuffle(male.get(i));
		        	male.set(i, (ArrayList<String>) male.get(i).subList(0, numBlogs));
	    		}
	    		if (numBlogs > 0 && numBlogs < female.size()) {
	        		Collections.shuffle(female.get(i));
	        		female .set(i,(ArrayList<String>)female.get(i).subList(0, numBlogs));
	        	}
	    		System.err.println("[Processing.runXtractOnGender] Sizes: M " + male.get(i).size() + ", F " + female.get(i).size());

    		}
    		
    		
        	System.err.println("[Processing.runXtractOnGender] Running Xtract on male");
        	Xtract xtract = new Xtract(_parser, outputDirectory + "/M/", "csubj|nn|dobj|amod",1,1,10,.75);
        	for (int i = 0; i < profilesDirectories.size(); i++) {
        		 runXtractP1(xtract, male.get(i), profilesDirectories.get(i), entriesDirectories.get(i), outputDirectory + "/M/", numBlogs, numWords, comparer, null, false);	
        	}
        	this.runXtractP2(xtract, outputDirectory + "/M/", comparer, amount, null);
        	// use a random set of bloggers
        	
        	System.err.println("[Processing.runXtractOnGender] Running Xtract on female");
        	xtract = new Xtract(_parser, outputDirectory + "/F/", "csubj|nn|dobj|amod",1,1,10,.75);
        	for (int i = 0; i < profilesDirectories.size(); i++) {
        		runXtractP1(xtract, female.get(i), profilesDirectories.get(i), entriesDirectories.get(i), outputDirectory + "/F/", numBlogs, numWords, comparer, null, false);	
        	}
        	this.runXtractP2(xtract, outputDirectory + "/F/", comparer, amount, null);	
        }
    
    public void runXtractOnTwitterDemographics(String politicsList, String entriesDirectory, String outputDirectory, int numBlogs, 
    		int numWords, String comparer, int amount, String type, String [] types) {
        	
        	HashMap<String, HashSet<String>> blogPolitics = new HashMap<String, HashSet<String>>();
        	for (String t : types) {
        		blogPolitics.put(t, new HashSet<String>());
        	}
        	
        	try {
            	BufferedReader in = new BufferedReader(new FileReader(politicsList));
     
                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                	String [] data = inputLine.split(",");
                	
                	if (!blogPolitics.containsKey(data[1])) continue;
                	blogPolitics.get(data[1]).add(data[0]);
                }
                in.close();
        	} catch (Exception e) {
        		System.err.println("[Processing.runXtractOnTwitterDemographics] " + e);
        		e.printStackTrace();
        	}
        	
    		
    		for (String t : blogPolitics.keySet()) {
    			List<String> people = new ArrayList<String>(blogPolitics.get(t));
        		System.err.println("[Processing.runXtractOnTwitterDemographics] Sizes: " + t + " " + people.size());
    			
        		if (numBlogs > 0 && people.size() > numBlogs) {
    	        	Collections.shuffle(people);
    	        	people = people.subList(0, numBlogs);
        		}
    		
	    		// use a random set of bloggers
	        	System.err.println("[Processing.runXtractOnTwitterDemographics] Running Xtract on " + t);
	        	Xtract xtract = new Xtract(_parser, outputDirectory + "/" + t + "/", "csubj|nn|dobj|amod",1,1,10,.75);
	        	this.runXtractP1(xtract, people, null, entriesDirectory, outputDirectory + "/" + t + "/", numBlogs, numWords, comparer, null, true);
	        	this.runXtractP2(xtract, outputDirectory + "/" + t + "/", comparer, amount, null);
    		}
        }

    public void runXtractOnAgeRanges(int year, List<String> ageLists, List<String> profilesDirectories, 
        	List<String> entriesDirectories, String outputDirectory, int numBlogs, int numWords, String comparer, int amount) {
        	
        	List<Hashtable<String, Integer>> blogAge = new ArrayList<Hashtable<String, Integer>>();
        	
        	for (int i = 0; i < ageLists.size(); i++) {
        		blogAge.add(new Hashtable<String,Integer>());
	        	try {
	            	BufferedReader in = new BufferedReader(new FileReader(ageLists.get(i)));
	     
	                String inputLine;
	                while ((inputLine = in.readLine()) != null) {
	                	String [] data = inputLine.split(",");
	                	if (data.length != 3) continue;
	                	blogAge.get(i).put(data[0],Integer.valueOf(data[1]));
	                }
	                in.close();
	        	} catch (Exception e) {
	        		System.err.println("[Processing.runXtract] " + e);
	        		e.printStackTrace();
	        	}
        	}
        	
    		List<List<String>> lower = new ArrayList<List<String>>();
    		List<List<String>> upper = new ArrayList<List<String>>();
    		
    		for (int i = 0; i < blogAge.size(); i++) {
    			
    			lower.add(new ArrayList<String>());
    			upper.add(new ArrayList<String>());
	    		Iterator<String> it = blogAge.get(i).keySet().iterator();
	    		
	    		while (it.hasNext()) {
	    			String blogger = (String)it.next();
	    			
	    			if (blogAge.get(i).get(blogger) >= year && blogAge.get(i).get(blogger) < 2000) upper.get(i).add(blogger);
	    			else if (blogAge.get(i).get(blogger) < year && blogAge.get(i).get(blogger) > 1940) lower.get(i).add(blogger);
	    		}
    		
	    		System.err.println("[Processing.runXtractOnAgeRanges] Sizes: >" + year + ": " + upper.get(i).size() + ", <" + year + ": " + lower.get(i).size());
	    		
	    		// use a random set of bloggers
	    		if (numBlogs > 0 && numBlogs < lower.get(i).size()) {
		        	Collections.shuffle(lower.get(i));
		        	lower.set(i, lower.get(i).subList(0, numBlogs));
	    		}
	    		// use a random set of bloggers
	        	if (numBlogs > 0 && numBlogs < upper.get(i).size()) {
	        		Collections.shuffle(upper.get(i));
	        		upper.set(i,upper.get(i).subList(0, numBlogs));
	        	}
    		}
    		
        	System.err.println("[Processing.runXtractOnAgeRanges] Running Xtract on <" + year);
        	Xtract xtract = new Xtract(_parser, outputDirectory + "/" + year + "/L/", "csubj|nn|dobj|amod",1,1,10,.75);
        	for (int i = 0; i < profilesDirectories.size(); i++) {
        		runXtractP1(xtract,lower.get(i), profilesDirectories.get(i), entriesDirectories.get(i), outputDirectory + "/" + year + "/L/", numBlogs, numWords, comparer, null, false);	
        	}
        	this.runXtractP2(xtract, outputDirectory + "/" + year + "/L/", comparer, amount, null);	
        	
        	
        	/*System.err.println("[Processing.runXtractOnAgeRanges] Running Xtract on >=" + year);
        	Xtract xtract = new Xtract(PARSER, outputDirectory + "/" + year + "/U/", "csubj|nn|dobj|amod",1,1,10,.75);
        	for (int i = 0; i < profilesDirectories.size(); i++) {
        		runXtractP1(xtract,upper.get(i), profilesDirectories.get(i), entriesDirectories.get(i), outputDirectory + "/" + year + "/U/", numBlogs, numWords, comparer, null, false);	
        	}
        	this.runXtractP2(xtract, outputDirectory + "/" + year + "/U/", comparer, amount, null);		*/
        }

    
    public void createProfileAgeList(String bloggerList, String profilesDirectory, String entriesDirectory) {
    	
    	try {
        	BufferedReader in = new BufferedReader(new FileReader(bloggerList));
        	BufferedWriter out = new BufferedWriter(new FileWriter(bloggerList + ".age"));
 
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
           		Blog blog = null;
        		if (profilesDirectory == null) 
             		 blog = Blog.processBlog(entriesDirectory + "/" + inputLine);
        		else
        			 blog = Blog.processBlog(profilesDirectory, entriesDirectory, inputLine);
        		
            	System.out.println(inputLine.replace(".xml", "") + " " + new SimpleDateFormat("yyyy").format(blog.getDOB()));
            	//if (blog.getLastBuildDate() == null || !blog.getCountry().trim().equals("US")) {
            	//	continue;
            	//}
            	
            	out.write(inputLine.replace(".xml", "") + "," + new SimpleDateFormat("yyyy").format(blog.getDOB()) + "," 
            			+ (Integer.valueOf(new SimpleDateFormat("yyyy").format(blog.getLastBuildDate())) - 
            			Integer.valueOf(new SimpleDateFormat("yyyy").format(blog.getDOB()))) + "\n");
            }
            in.close();
            out.close();
    	} catch (Exception e) {
    		System.err.println("[Processing.createProfielAgeList] " + e);
    		e.printStackTrace();
    	}
    }
    
    public void createProfileGenderList(Label label, String bloggerList, String profilesDirectory, String entriesDirectory) {
    	
    	try {
        	BufferedReader in = new BufferedReader(new FileReader(bloggerList));
        	BufferedWriter out = new BufferedWriter(new FileWriter(bloggerList + ".gender"));
 
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
          		Blog blog = null;
        		if (profilesDirectory == null) 
             		 blog = Blog.processBlog(entriesDirectory + "/" + inputLine);
        		else
        			 blog = Blog.processBlog(profilesDirectory, entriesDirectory, inputLine);
        		
            	//if (blog.getLastBuildDate() == null || !blog.getCountry().trim().equals("US")) {
            	//	continue;
            	//}
            	String gender = blog.getGender() != null ? blog.getGender() : label.getGender(blog);
            	if (gender.equals("U")) continue;
            	System.out.println(inputLine.replace(".xml", "") + " " + gender);
            	out.write(inputLine.replace(".xml", "") + "," + gender + "\n");
            }
            in.close();
            out.close();
    	} catch (Exception e) {
    		System.err.println("[Processing.createProfielAgeList] " + e);
    		e.printStackTrace();
    	}
    }
    
    public void runXtractP1(Xtract xtract, List<String> bloggers, String profilesDirectory, 
    		String entriesDirectory, String outputDirectory, int numBlogs, int numWords, 
    		String comparer, Hashtable<String,Integer> interests, boolean twitter) {
    	
    	new File(outputDirectory).mkdirs();

    	int i = 0;
    	
    	for (String blogger : bloggers) {
    		if (i % 100 == 0) System.out.println(i + "/" + bloggers.size());
    		/*System.out.println(blogger);
    		if (blogger.equals("292083654")) {
    			System.out.println("-- break --");
    		}*/
    		Blog blog = null;
    		if (profilesDirectory == null) 
         		 blog = Blog.processBlog(entriesDirectory + "/" + blogger + ".xml",100,0,144, numWords);
    		else
    			 blog = Blog.processBlog(profilesDirectory, entriesDirectory, blogger,100,0,144, numWords);
    		this.addToXtract(xtract, blog, numWords, comparer, interests, twitter);
    		i++;
    		if (i > numBlogs) break;
    	}
    }
    
    public void runXtractP2(Xtract xtract, String outputDirectory, String comparer, int amount, Hashtable<String,Integer> interests) {
    	
    	xtract.run();
    	this.printGrams(xtract, outputDirectory, amount, comparer);
    	this.writeHash(interests, outputDirectory + "/interests.txt", amount);
    }
    
    private int addToXtract(Xtract xtract, Blog blog, int numWords, String comparer, Hashtable<String,Integer> interests, boolean twitter) {
		
    	// calculate top interests too
    	if (interests != null)
    		this.processInterests(interests, blog);

		Iterator<String> it = blog.getEntries();						
		int wordCount = 0;
		
		while (it.hasNext() && wordCount < numWords) {
        	Entry e = (Entry)blog.getEntry((String)it.next());
        	
        	// convert to collocation sentence
        	ArrayList<blog.Sentence> sentences = e.getSentences();
        	ArrayList<collocation.Sentence> xSentences = new ArrayList<collocation.Sentence>(); 
        	
        	for (int index = 0; index < sentences.size(); index++) {
        		blog.Sentence s = sentences.get(index);

        		if (s == null || s.getText() == null) continue;
        		//String sentence = s.getSentence().replaceAll("\\p{Punct}/\\p{Punct}", "");
        		String sentence = convertLinks(s.getSentence(),twitter).trim();
        		if (sentence.isEmpty()) continue;
        		xSentences.add(new collocation.Sentence(sentence,s.getDependencies(), 
        				comparer.equals("ef") ? e.getURL().toString() : blog.getUsername()));
        		wordCount += s.getNumWords();
        		if (wordCount > numWords) break;
        	}
        	
        	xtract.addDocument(xSentences);
		}
		return wordCount;
	}
    
    private void printGrams(Xtract xtract, String outputDirectory, int amount, String comparer) {
    	
    	this.writeBigramsToFile(xtract.getBigrams(amount, comparer, "text"),outputDirectory + "/bigrams-" + comparer + ".txt", false);
    	this.writeBigramsToFile(xtract.getSyntaxBigrams(amount, comparer, "text"),outputDirectory + "/syntax-bigrams-" + comparer + ".txt", false);
    	this.writeBigramsToFile(xtract.getBigrams(amount, comparer, "pos"),outputDirectory + "/bigrams-pos-" + comparer + ".txt", true);
    	this.writeBigramsToFile(xtract.getSyntaxBigrams(amount, comparer, "pos"),outputDirectory + "/syntax-bigrams-pos-" + comparer + ".txt", false);
		this.writeGramsToFile(xtract.getGrams(amount, comparer),outputDirectory + "/grams-" + comparer + ".txt");
    }
    
    private void writeBigramsToFile(List<XtractBigram> bigrams, String outputFile, boolean pos) {
    	
    	try {
			BufferedWriter out = new BufferedWriter(new FileWriter(outputFile));
	    	
			for (XtractBigram bigram : bigrams) {
	    		if (pos) out.write(bigram.getFrequency() + "," + bigram.getDocumentFrequency() + "," + bigram.getBigram() + 
	    				"," + bigram.getBigramAsPOSRegularExpression() + "\n");
	    		else out.write(bigram.getFrequency() + "," + bigram.getDocumentFrequency() + "," + bigram.getBigram() + "," 
	    				+ bigram.getBigramAsRegularExpression() + "\n");
	    	}
			out.close();
    	} catch (Exception e) {
    		System.err.println("[Processing.writeBigramsToFile] " + e);
    	}
    }
    
    private void writeGramsToFile(List<XtractGram> grams, String outputFile) {
    	
    	try {
			BufferedWriter out = new BufferedWriter(new FileWriter(outputFile));
	    	
			for (XtractGram gram : grams) {
	    		out.write(gram.getFrequency() + "," + gram.getDocumentFrequency() + "," + gram.getWord() + "\n");
	    	}
			out.close();
    	} catch (Exception e) {
    		System.err.println("[Processing.writeBigramsToFile] " + e);
    	}
    }
    
    /**
     * Write data to file
     * @param hashtable
     * @param outputFile
     */
    private void writeHash(Hashtable<String,Integer> hashtable, String outputFile, int amount) {
    	    	
    	try {
    		
    		List<HashClass> values = this.convertHash(hashtable);

	    	Collections.sort(values);
	    	
	    	BufferedWriter out = new BufferedWriter(new FileWriter(outputFile));
	    	
	    	for (int index = 0; index < amount && index < values.size(); index++) {
	    		out.write(values.get(index) + "\n");
	    	}
	    	
	    	out.close();
    	} catch (Exception e) {
    		e.printStackTrace();
    		System.err.println("[Processing.writeHash] " + e);
    	}
    } 
    
    private void processInterests(Hashtable<String,Integer> interests, Blog blog) {
		
    	Iterator<String> it = blog.getInterests();
    	
    	while (it.hasNext()) {
    		String interest = (String)it.next();
    		
    		interests.put(interest, interests.containsKey(interest) ? interests.get(interest) + 1 : 1);
    	}
	}
    
    public static void computeCounts(String ageList, String inputDirectory) {
    	HashSet<String> list = blog.threaded.livejournal.LiveJournalRobot.loadProfileList(ageList);
    	String [] ageRanges  = {"18-22","28-32","38-42"};
    	int entryCount = 0;
    	//int wordCount = 0;
    	
    	for (String info : list) {
    		String [] data = info.split(",");
    		if (getRange(Integer.valueOf(data[2]),ageRanges) < 0) continue;
    		Blog blog = Blog.processBlog(inputDirectory, inputDirectory, data[0]);
    		entryCount += blog.getNumEntries();
    	}
    	System.out.println("There are " + entryCount + " entries.");
    }
    
    private static int getRange(int year, String ageRanges[]) {
		
		for (int index = 0; index < ageRanges.length; index++) {
			int dash = ageRanges[index].indexOf("-");
			int start = Integer.valueOf(ageRanges[index].substring(0, dash));
			int end = Integer.valueOf(ageRanges[index].substring(dash + 1));
			
			if (year >= start && year <= end) return index;
		}
		return -1;
	}
    
    public void countryRemove(String bloggerList, String profilesDirectory, String entriesDirectory) {
    	
    	try {
        	BufferedReader in = new BufferedReader(new FileReader(bloggerList));
        	BufferedWriter out = new BufferedWriter(new FileWriter(bloggerList + ".us"));
 
            String inputLine;
            
            while ((inputLine = in.readLine()) != null) {
            	Blog blog = Blog.processBlog(profilesDirectory, entriesDirectory, inputLine.split(",")[0]);
            	if (blog.getCountry().equals("US")) {
            		out.write(inputLine + "\n");
            	}
            }
            in.close();
            out.close();
    	} catch (Exception e) {
    		System.err.println("[Processing.countryCount] " + e);
    		e.printStackTrace();
    	}
    }
    
    public void detectLanguage(String bloggerList, String profilesDirectory, String entriesDirectory) {
    	
    	try {
        	BufferedReader in = new BufferedReader(new FileReader(bloggerList));
        	
            String inputLine;
            
            int count = 0;
            
            while ((inputLine = in.readLine()) != null && count < 500) {
            	Blog blog = Blog.processBlog(profilesDirectory, entriesDirectory, inputLine.split(",")[0]);
            	if (blog.getBio() == null) continue;
            	String text = blog.getBio().replaceAll("\n", " ");
            	System.out.println(count + ". " + inputLine.split(",")[0] + ": " + (text.length() > 100 ? text.substring(0,100) : text));
            	count++;
            }
            in.close();
    	} catch (Exception e) {
    		System.err.println("[Processing.detectLanguage] " + e);
    		e.printStackTrace();
    	}
    }
    
    public static String convertLinks(String text, boolean twitter) {
		text = text.replaceAll("http:", "LLLINKKK ");
		
		// remove twitter @ and #
		if (twitter) {
			text = text.replaceAll("@\\w+\\b+", "UUUSERRR"); //@user
			text = text.replaceAll("#\\w+\\b+", "HHHASHTAGGG"); // #hashtag
			//text = text.replaceAll("\\b+RT/NN\\b+", ""); // Retweet
			//text = text.replaceAll("\\s+", " ");
		}
		return text;
	}
	
    public static void fixSyntaxBigramPOS(String file) {
    	
    	try {
    		BufferedReader input = new BufferedReader(new FileReader(file));
    		BufferedWriter output = new BufferedWriter(new FileWriter(file + ".new"));
    		
    		String inputLine = "";
    		
    		// 16689,it vb,vb \p{Graph}+ \p{Graph}+ \p{Graph}+ \p{Graph}+ it|vb \p{Graph}+ \p{Graph}+ \p{Graph}+ it|
    		// vb \p{Graph}+ \p{Graph}+ it|vb \p{Graph}+ it|vb it|it vb
    		
    		while ((inputLine = input.readLine()) != null) {
    			String [] data = inputLine.split(",");
    			String searchPhrase = data[data.length-1];
    			
      			String outputLine = "";
      			String [] phrases = searchPhrase.split("\\|");
      			
      			for (String phrase : phrases) {
      				String [] words = phrase.split("\\s+");
      				
      				for (String word : words) {
      					if (word.matches("vb")) outputLine += "\\p{Graph}+/vb ";
      					else if (word.equals("nn")) outputLine += "\\p{Graph}+/nn ";
      					else if (word.equals("jj")) outputLine += "\\p{Graph}+/jj ";
      					else if (word.equals("dt")) outputLine += "\\p{Graph}+/dt ";
      					else if (word.equals("rb")) outputLine += "\\p{Graph}+/rb ";
      					else if (word.equals("prp")) outputLine += "\\p{Graph}+/prp ";
      					else if (word.equals("\\p{Graph}+")) outputLine += word + " ";
      					else outputLine += word + "/" + "\\p{Graph}+ ";
      				}
      				outputLine = outputLine.trim();
      				outputLine += "|";
      			}
      			
    			output.write(data[0] + "," + data[1] + (data.length == 4 ? "," + data[2] : "") + "," + outputLine.trim().substring(0,outputLine.length()-1) + "\n");
    		}
    		input.close();
    		output.close();
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
    
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		/*
		 * 1. add POS and Dependencies to Blogs
		 * 2. run xtract on different author trait datasets
		 * 3. xtract output and processed files are ready. Time to run FastWeka.java!!
		 */
		System.err.println("START: " + new Date());
		
		String action = "";
		String inputDirectory = "";
		String outputDirectory = "";
		String type = "";
		int numBloggers = 0;
		String list = "";
		String processed = "";
		String profiles = "";
		String freq = "ef"; // comparers: tf, ef, bf
		int min = 0;
		int max = 0;
		
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-action")) {
				action = args[++i];
			}
			if (args[i].equals("-input")) {
				inputDirectory = args[++i];
			}
			if (args[i].equals("-output")) {
				outputDirectory = args[++i];
			}
			// list of users
			if (args[i].equals("-list")) {
				list = args[++i];
			}
			// processed folder
			if (args[i].equals("-processed")) {
				processed = args[++i];
			}
			// list of profile directories
			if (args[i].equals("-profiles")) {
				profiles = args[++i];
			}
			if (args[i].equals("-type")) {
				type = args[++i];
			}
			if (args[i].equals("-nb")) {
				numBloggers = Integer.valueOf(args[++i]);
			}
			if (args[i].equals("-min")) {
				min = Integer.valueOf(args[++i]);
			}
			if (args[i].equals("-max")) {
				max = Integer.valueOf(args[++i]);
			}
		}
		
		Properties prop = GeneralUtils.getProperties("config/config.properties");
		ArrayList<String> profileDirectories = new ArrayList<String>();
		ArrayList<String> userLists = new ArrayList<String>();
		ArrayList<String> entriesDirectories = new ArrayList<String>();
		
		if (type.equals("gender")) {
			userLists.add(prop.getProperty("profileList_gender_lj"));
			userLists.add(prop.getProperty("profileList_gender_authorship"));
			profileDirectories.add(prop.getProperty("LJdirectory"));
			profileDirectories.add(null);
			entriesDirectories.add(prop.getProperty("processeddirectory_lj"));
			entriesDirectories.add(prop.getProperty("processeddirectory_gender_authorship"));
		}
		else if (type.equals("age") || type.equals("year")) {
			userLists.add(prop.getProperty("profileList_age_lj"));
			profileDirectories.add(prop.getProperty("LJdirectory"));
			userLists.add(prop.getProperty("profileList_age_authorship"));
			entriesDirectories.add(prop.getProperty("processeddirectory_lj"));
			entriesDirectories.add(prop.getProperty("processeddirectory_gender_authorship"));
		}
		else if (type.equals("politics")) {
			userLists.add(prop.getProperty("profileList_politics_twitter"));
			entriesDirectories.add(prop.getProperty("processeddirectory_politics_twitter"));
		}
		else if (type.equals("religion")) {
			userLists.add(prop.getProperty("profileList_religion_twitter"));
			entriesDirectories.add(prop.getProperty("processeddirectory_religion_twitter"));
		}
		/*input:
		 * 	"/proj/nlp/users/sara/corpora/blogs/livejournal/2010-corpus/";
			"/proj/nlp/users/sara/demographics/corpus/";
			"/proj/nlp/users/sara/corpora/blogs/authorship/";
		  output: "/proj/nlp/users/sara/demographics/";
		  bloggerList: 
		  	"corpus/year/profile_test_list.txt";
		 	"entries/profile_testing.txt";
		  ageList:
			"/year/profile_train_list.txt.year";
			"entries/profile_training.txt.age";
		  genderList:
			"/gender/profile_train_list.txt.gender";
			"entries/profile_training.txt.gender";
		*/
		
		/*List<String> entriesDirectory = new ArrayList<String>();
		entriesDirectory.add("/proj/nlp/users/sara/demographics/processed/livejournal/");
		entriesDirectory.add(inputDirectory + "/processed/");*/
		
		//bloggerList = directory + "corpus/religion.txt";
		
		Processing p = new Processing();
		
		System.out.println("Running Action: " + action);
		
		// should not be needed
		/*if (args[0].equals("country")) {
			p.countryRemove(list, inputDirectory, null);
		}
		if (action.equals("lang-detect")) {
			p.detectLanguage(list + ".us",inputDirectory,inputDirectory);
		}*/
		if (action.equals("compute-ages")) {
			p.createProfileAgeList(list, profiles, processed);
		}
		if (action.equals("compute-gender")) {
			Label label = new Label();
			p.createProfileGenderList(label, list, profiles, processed);
		}
		else if (action.equals("add-pos-dep")) {
			p.addPOSandDependencies(null, inputDirectory + "/entries/", 
					outputDirectory + "/processed/", min, max);
		}
		else if (action.equals("xtract")) {

			for (int index = 0; index < entriesDirectories.size(); index++) {
				String file = entriesDirectories.get(index);
				if (!new File(file).exists()) {
					p.addPOSandDependencies(profileDirectories.get(0), file, file, 0, 60000);
				}
			}
			
			if (type.equals("politics") || type.equals("twitter")) {
				String [] types = null;
				
				if (type.equals("religion")) {
					String [] t = {"judaism","islam","christian","atheist"}; //"hinduism","buddhism"}; 
					types = t;
				}
				if (type.equals("politics")) {
					String [] t = {"republican","democrat"};
					types = t;
				}
				p.runXtractOnTwitterDemographics(userLists.get(0), entriesDirectories.get(0), 
						outputDirectory + "/xtract/politics", numBloggers, 20000, freq, 10000, type, types);
			}
			else if (type.equals("gender")) {
				p.runXtractOnGender(userLists, profileDirectories, entriesDirectories, outputDirectory + "/xtract/gender/", numBloggers, 1000, freq, 10000);
			}
			else if (type.equals("year")) {
				int year = 1982; 
				//groups: 1975-1986 (split)
				
				p.runXtractOnAgeRanges(year, userLists, profileDirectories, entriesDirectories, outputDirectory + "/xtract/year/", numBloggers, 1000, freq, 10000);
			}
			else if (type.equals("age")) {
				// groups: 18-22, 28-32, 38-42
				p.runXtractOnAgeGroups(min, max, userLists, profileDirectories, entriesDirectories, outputDirectory + "/xtract/age", numBloggers, 1000, freq, 500);
			}
		}
		// should not be neededs
		/*else if (args[0].equals("fix-pos")) {
			Processing.fixSyntaxBigramPOS("/proj/nlp/users/sara/demographics/xtract-2014/religion/atheist/syntax-bigrams-pos-ef.txt");
		}*/
		System.err.println("END: " + new Date());	
	}

}
