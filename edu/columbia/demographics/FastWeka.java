package edu.columbia.demographics;

import java.util.*;
import java.util.regex.*;

import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.core.converters.ArffSaver;

import java.io.*;

import edu.columbia.utilities.liwc.LIWC;
import edu.columbia.utilities.weka.ChiSquareFeatureSelection;
import edu.columbia.utilities.weka.InstancesProcessor;

import processing.GeneralUtils;
import social_media.Features;

import blog.threaded.livejournal.*;

/**
 * Class to gather features and generate arff file for weka
 * @author sara
 *
 */

public class FastWeka {

	String profilesDirectory;
	String entriesDirectory;
	String profileTestList;
	String profileList;
	Hashtable<String,String> bigrams;
	List<String> blogs;
	List<String> testBlogs;
	Hashtable<String,String> profiles;
	HashSet<String> dictionary;
	HashSet<String> emoticons;
	HashSet<String> acronyms;
	
	edu.columbia.demographics.gender.Label _gender;
	Features _smFeatures;
	String outputDirectory;
	String minAge;
	String maxAge;
	String [] ageRanges;
	String year;
	String rangeType;
	LIWC _liwc;
	int _max = 100;
	boolean _boolean = false;
	String _arffType = "twitter";
	public static boolean DEBUG = false;

	static final int YEAR = 0;
	static final int FRIENDS = 1;
	static final int POSTS = 2;
	static final int SM = 3;
	static final int LIWC = 4;
	static final int COMMENTS = 6;
	static final int ENTRIES = 7;
	static final int AVG_TIME = 8;
	static final int MODE_TIME = 9;
	static final int POS = 13;
	static final int INTERESTS = 14;
	static final int BIGRAMS = 15;
	static final int SYNTAX_BIGRAMS = 16;	
	static final int GRAMS = 17;
	static final int BIGRAMS_POS = 18;
	static final int SYNTAX_BIGRAMS_POS = 19;
	static final int AVG_DAY = 20;
	static final int MODE_DAY = 21;
	static final int GENDER = 22;
	static final int POLITICAL_VIEW = 23;
	static final int POLITICAL_PARTY = 24;
	static final int RELIGION = 25;
	public static final String NUM_WORDS = "SM_word_count";
	boolean _twitter = false;
	
	public static String valueOf(int att) {
		switch (att) {
			case YEAR: return "YEAR";
			case GENDER: return "GENDER";
			case POLITICAL_PARTY: return "POLITICAL_PARTY";
			case POLITICAL_VIEW: return "POLITICAL_VIEW";
			case RELIGION: return "RELIGION";
			case FRIENDS: return "FRIENDS";
			case POSTS: return "POSTS";
			case 3: return "PUNCTUATION";
			case 4: return "CAPITALIZATIONS";
			case 5: return "SENTENCE_LENGTH";
			case COMMENTS: return "COMMENTS";
			case ENTRIES: return "ENTRIES";
			case AVG_TIME: return "AVG_TIME";
			case MODE_TIME: return "MODE_TIME";
			case AVG_DAY: return "AVG_DAY";
			case MODE_DAY: return "MODE_DAY";
			case 10: return "SLANG";
			case 11: return "EMOTICONS";
			case 12: return "ACRONYMS";
			case 13: return "LINKS";
			case INTERESTS: return "INTEREST";
			case BIGRAMS: return "BIGRAM";
			case SYNTAX_BIGRAMS: return "SYNTAX_BIGRAM";
			case GRAMS: return "GRAM";
			case BIGRAMS_POS: return "POS_BIGRAM";
			case SYNTAX_BIGRAMS_POS: return "POS_SYNTAX_BIGRAM";			
		}
		return null;
	}
	
	public static int valueOf(String att) {
		
		if (att.equals("YEAR")) return YEAR;
		if (att.equals("GENDER")) return GENDER;
		if (att.equals("POLITICAL_VIEW")) return POLITICAL_VIEW;
		if (att.equals("POLITICAL_PARTY")) return POLITICAL_PARTY;
		if (att.equals("RELIGION")) return RELIGION;
		if (att.equals("FRIENDS")) return FRIENDS;
		if (att.equals("POSTS")) return POSTS;
		if (att.equals("COMMENTS")) return COMMENTS;
		if (att.equals("ENTRIES")) return ENTRIES;
		if (att.equals("AVG_TIME")) return AVG_TIME;
		if (att.equals("MODE_TIME")) return MODE_TIME;
		if (att.equals("AVG_DAY")) return AVG_DAY;
		if (att.equals("MODE_DAY")) return MODE_DAY;
		if (att.equals("INTEREST")) return INTERESTS;
		if (att.equals("BIGRAM")) return BIGRAMS;
		if (att.equals("SYNTAX_BIGRAM")) return SYNTAX_BIGRAMS;
		if (att.equals("GRAM")) return GRAMS;
		if (att.equals("POS_BIGRAM")) return BIGRAMS_POS;
		if (att.equals("POS_SYNTAX_BIGRAM")) return SYNTAX_BIGRAMS_POS;			
		return -1;
	}
	
	public static String code(int att) {
		switch (att) {
			case YEAR: return "Y";
			case GENDER: return "G";
			case POLITICAL_VIEW: return "Pv";
			case POLITICAL_PARTY: return "Pp";
			case RELIGION: return "R";
			case FRIENDS: return "F";
			case POSTS: return "Po";
			case 3: return "Pu";
			case 4: return "Cap";
			case 5: return "SeLe";
			case COMMENTS: return "Com";
			case ENTRIES: return "En";
			case AVG_TIME: return "ATi";
			case MODE_TIME: return "MTi";
			case AVG_DAY: return "ADa";
			case MODE_DAY: return "MDa";			
			case 10: return "Sl";
			case 11: return "Emo";
			case 12: return "Acr";
			case 13: return "Li";
			case INTERESTS: return "I";
			case BIGRAMS: return "B";
			case SYNTAX_BIGRAMS: return "SB";
			case GRAMS: return "BOW";
			case BIGRAMS_POS: return "PB";
			case SYNTAX_BIGRAMS_POS: return "PSB";			
		}
		return null;
	}
	
	/**
	 * Initalize 
	 * @param outputDirectoryIn
	 * @param profilesDirectoryIn
	 * @param entriesDirectoryIn
	 * @param testEntriesDirectoryIn
	 * @param ageRangesIn
	 */
	public FastWeka(String outputDirectoryIn, String profilesDirectoryIn, String entriesDirectoryIn,
			String [] ageRangesIn, String rangeTypeIn, String yearIn, String profileListIn, String profileTestListIn, String source) {
		
		year = yearIn;
		bigrams = new Hashtable<String,String>();
		ageRanges = ageRangesIn;
		rangeType = rangeTypeIn;
		outputDirectory = outputDirectoryIn;
   		profilesDirectory = profilesDirectoryIn;
   		entriesDirectory = entriesDirectoryIn;
   		profileTestList = profileTestListIn;
   		profileList = profileListIn;
   		_gender = new edu.columbia.demographics.gender.Label();
   		
   		Properties prop = GeneralUtils.getProperties("config/config.properties");
   		if (new File(prop.getProperty("liwc")).exists())
   			_liwc = new LIWC(prop.getProperty("liwc"));
		String acronymFile = prop.getProperty("emoticons_directory") + "/acronyms.txt";
		String dictionaryFile = prop.getProperty("dictionary");
   		_smFeatures = new Features(dictionaryFile, prop.getProperty("emoticons_directory") + "/emoticon_sub.txt", acronymFile);
   		
   		_arffType = source;
		if (source.equals("authorship") || source.equals("lj")) {
			_max = 25;
			_twitter = false;
		}
		else {
			_max = 100;
			_twitter = true;
		}
 	}
	
	public Instances loadInstances(String file) {
		
		Instances data = null;
		
		try {
			data = DataSource.read(file.toString());
		    data.setClassIndex(data.numAttributes() - 1);
		} catch (Exception e) {
			System.err.println("[FastWeka.loadInstances] " + file.toString() + ": " + e);
			e.printStackTrace();
		}
	    return data;
	}
	
	public void run(List<Integer> attributesKeep, int gramAmount, int numBloggers, String xtractDirectory, String comparer, int start, boolean balanced) {
		try {
			Properties prop = GeneralUtils.getProperties("config/config.properties");
			String emoticonFile = prop.getProperty("emoticons_directory") + "/emoticons.txt.codes";
			String acronymFile = prop.getProperty("emoticons_directory") + "/acronyms.txt";
			String dictionaryFile = prop.getProperty("dictionary");
			
			
			this.run(attributesKeep, gramAmount, numBloggers, xtractDirectory, dictionaryFile, emoticonFile, acronymFile, comparer, start, balanced);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void run(List<Integer> attributesKeep, int gramAmount, int numBloggers, String xtractDirectory, String dictionaryFile, 
			String emoticonFile, String acronymFile, String comparer, int start, boolean balanced) {
   		   		
		System.err.print("[FastWeka.run] (" + new java.text.SimpleDateFormat("hh:mm aaa").format(new Date()) + ") Load training and test set... ");
   		// load training set
   		blogs = profileList != null ? loadTrainingSet(profileList, numBloggers, start, balanced) : null;
		// load test set
		testBlogs = profileTestList != null ? loadTestSet(profileTestList, numBloggers, start, balanced) : null;
		System.err.println("DONE");
		
		// load data
		System.err.print("[FastWeka.run] (" + new java.text.SimpleDateFormat("hh:mm aaa").format(new Date()) + ") load data files (emoticons, acronyms, dictionary)... ");
		try {
			emoticons = new HashSet<String>(processing.SwapEmoticons.readCodes(new File(emoticonFile), false).values());
			acronyms = loadDataFile(acronymFile);
			dictionary  = loadDataFile(dictionaryFile);
		} catch (Exception e) {
			System.err.println("[FastWeka.run] " + e);
   			e.printStackTrace();
		}
		System.err.println("DONE");
		
		// load all attributes - including grams, bigrams, syntax bigrams
		System.err.println("[FastWeka.run] (" + new java.text.SimpleDateFormat("hh:mm aaa").format(new Date()) + ") load attributes... "); 
		
		ArrayList<Attribute> attributes = loadAttributes(ageRanges, attributesKeep,xtractDirectory, gramAmount, comparer, false);
   		System.err.println("DONE");

   		// create Instances and fill with data
   		System.err.println("[FastWeka.run] (" + new java.text.SimpleDateFormat("hh:mm aaa").format(new Date()) + ") compute instances... "); 
   		
   		
   		if (blogs != null) {
	   		Instances instances = getInstances(attributes, blogs);
	 		System.err.println("DONE");
	   		System.err.println("[FastWeka.run] (" + new java.text.SimpleDateFormat("hh:mm aaa").format(new Date()) + ") write to arff file... "); 
	   		this.writeToFile(instances, outputDirectory + "/train/" + rangeType + (year != null ? "-" + year : "") + "." + comparer + "." + start + "-" + (start+numBloggers) + "." + _arffType + ".arff");
	   		System.out.println("Writing to: " + outputDirectory + "/train/" + rangeType + (year != null ? "-" + year : "") + "." + comparer + "." + start + "-" + (start+numBloggers) + "." + _arffType + ".arff");
	   		System.err.println("DONE");
   		}  		
   		if (testBlogs != null) {
   			try {
	   			Instances testInstances = getInstances(attributes, testBlogs);
	   	 		this.writeToFile(testInstances, outputDirectory + "/test/" + rangeType + (year != null ? "-" + year : "") + "." + comparer + "." + start + "-" + (start+numBloggers) + "." + _arffType + ".arff");
	   	   		System.out.println("Writing to: " + outputDirectory + "/test/" + rangeType + (year != null ? "-" + year : "") + "." + comparer + "." + start + "-" + (start+numBloggers) + "." + _arffType + ".arff");
	   	  	   	   		System.err.println("DONE");
   			} catch (Exception e) {
   				e.printStackTrace();
   			}
   		}
	}
	
	public void loadAndFilter(String trainArff, String testArff) {
		
		try {
			Instances train = DataSource.read(trainArff.toString());
			Instances test = DataSource.read(testArff.toString());
			applyChiSquare(trainArff, train, testArff, test);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void reduceGrams(String trainArff, String testArff, String xtractDirectory, int gramAmount) {
		
		try {
			System.err.print("[FastWeka.run] (" + new java.text.SimpleDateFormat("hh:mm aaa").format(new Date()) + ") Load training and test set... ");
			Instances train = DataSource.read(trainArff.toString());
			Instances test = DataSource.read(testArff.toString());

			Integer [] gramFeatures = {BIGRAMS,BIGRAMS_POS,GRAMS,SYNTAX_BIGRAMS,SYNTAX_BIGRAMS_POS};
			List<Integer> attributesKeep = Arrays.asList(gramFeatures);
			
			// load all attributes - including grams, bigrams, syntax bigrams
			System.err.print("[FastWeka.run] (" + new java.text.SimpleDateFormat("hh:mm aaa").format(new Date()) + ") load attributes... "); 
			
			ArrayList<Attribute> attributes = loadAttributes(ageRanges, attributesKeep,xtractDirectory, gramAmount, "ef", false);
	   		System.err.println("DONE");

	   		// create Instances and fill with data
	   		System.err.println("[FastWeka.run] (" + new java.text.SimpleDateFormat("hh:mm aaa").format(new Date()) + ") compute instances... "); 
	   		train = removeAttributes(attributes, train);
	   		test = removeAttributes(attributes, test);
	   		System.err.println("DONE");
	   		System.err.println("[FastWeka.run] (" + new java.text.SimpleDateFormat("hh:mm aaa").format(new Date()) + ") write to arff file... "); 
	   		this.writeToFile(train, trainArff + "." + gramAmount + "F.arff");
	   		this.writeToFile(test, testArff + "." + gramAmount + "F.arff");
	   		System.err.println("DONE");

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public Instances removeAttributes(ArrayList<Attribute> attributes, Instances instances) {
		HashSet<String> attributeNames = new HashSet<String>();
		
		for (int index = 0; index < attributes.size(); index++) {
			attributeNames.add(((Attribute)attributes.get(index)).name());
		}
		
		String remove = "";
		
		for (int index = 0; index < instances.numAttributes(); index++) {
			String attribute = instances.attribute(index).name();
			if (attribute.startsWith("GRAM") || attribute.startsWith("POS_BIGRAM") || attribute.startsWith("BIGRAM") ||
				 attribute.startsWith("SYNTAX_BIGRAM") || attribute.startsWith("POS_SYNTAX_BIGRAM")) {
				// keep!
				if (attributeNames.contains(attribute)) continue;
				// delete!
				remove += (index + 1) + ",";
			}
		}
		
		try {
			instances = InstancesProcessor.removeList(instances, remove);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return instances;
	}
	
	public void applyChiSquare(String trainFile, Instances train, String testFile, Instances test) {
		
   		HashSet<String> liwcGrams = new HashSet<String>();
   		HashSet<String> smGrams = new HashSet<String>();
   		HashSet<String> posGrams = new HashSet<String>();
   		HashSet<String> grams = new HashSet<String>();
   		HashSet<String> bigrams = new HashSet<String>();
   		HashSet<String> posbigrams = new HashSet<String>();
   		HashSet<String> syntaxbigrams = new HashSet<String>();
   		HashSet<String> syntaxposbigrams = new HashSet<String>();
   		
   		// need nominal class
   		if (rangeType.equals("year")) {
   			ArrayList<String> classes = new ArrayList<String>();
			classes.add("<" + year);
			classes.add(">=" + year);
			Attribute age = new Attribute("AGE", classes);
			train.insertAttributeAt(age, train.numAttributes()-1);
			if (test != null) test.insertAttributeAt(age, test.numAttributes()-1);
			
			for (int i = 0; i < train.numInstances(); i++) {
				double a = train.instance(i).value(train.attribute("YEAR"));
				if (a >= Integer.valueOf(year)) train.instance(i).setValue(train.attribute("AGE"), 1);
				else train.instance(i).setValue(train.attribute("AGE"), 0);
			}
			for (int i = 0; test != null && i < test.numInstances(); i++) {
				double a = test.instance(i).value(test.attribute("YEAR"));
				if (a > Integer.valueOf(year)) test.instance(i).setValue(test.attribute("AGE"), 1);
				else test.instance(i).setValue(test.attribute("AGE"), 0);
			}
   		}
   		
   		String classLabel = rangeType.toUpperCase();
   		if (rangeType.equals("year")) classLabel = "AGE";
   		
   		/**
		 * Perform chi square
		 */
		try {
			train = ChiSquareFeatureSelection.chiSquareByName(train, "LIWC", classLabel, null, true);
			liwcGrams = ChiSquareFeatureSelection.setNgramsByName(train, "LIWC");
			train = ChiSquareFeatureSelection.chiSquareByName(train, "SM", classLabel, null, true);
			smGrams = ChiSquareFeatureSelection.setNgramsByName(train, "SM");
			train = ChiSquareFeatureSelection.chiSquareByName(train, "POS ", classLabel);
			posGrams = ChiSquareFeatureSelection.setNgramsByName(train, "POS ");
			train = ChiSquareFeatureSelection.chiSquareByName(train, "POS_BIGRAM ", classLabel);
			posbigrams = ChiSquareFeatureSelection.setNgramsByName(train, "POS_BIGRAM ");
			train = ChiSquareFeatureSelection.chiSquareByName(train, "POS_SYNTAX_BIGRAM ", classLabel);
			syntaxposbigrams = ChiSquareFeatureSelection.setNgramsByName(train, "POS_SYNTAX_BIGRAM ");
			train = ChiSquareFeatureSelection.chiSquareByName(train, "SYNTAX_BIGRAM ", classLabel, null, true);
			syntaxbigrams = ChiSquareFeatureSelection.setNgramsByName(train, "SYNTAX_BIGRAM ");
			train = ChiSquareFeatureSelection.chiSquareByName(train, "BIGRAM ", classLabel);
			bigrams = ChiSquareFeatureSelection.setNgramsByName(train, "BIGRAM ");
			train = ChiSquareFeatureSelection.chiSquareByName(train, "GRAM ", classLabel); //, null, true);
			grams = ChiSquareFeatureSelection.setNgramsByName(train, "GRAM ");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
   		
		if (rangeType.equals("year")) {
			try {
				train = InstancesProcessor.removeList(train, String.valueOf(train.attribute("AGE").index()+1));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
   		System.err.println("DONE");
   		System.err.println("[FastWeka.run] (" + new java.text.SimpleDateFormat("hh:mm aaa").format(new Date()) + ") write to arff file... "); 
   		this.writeToFile(train, trainFile + "-filtered.arff");
   		System.out.println("Writing to: " + trainFile + "-filtered.arff");
   		System.err.println("DONE");
   		
   		if (test != null) {
   			try {
	   			if (!liwcGrams.isEmpty()) test = ChiSquareFeatureSelection.chiSquareTestByName(test, liwcGrams, "LIWC");
	   			if (!smGrams.isEmpty()) test = ChiSquareFeatureSelection.chiSquareTestByName(test, smGrams, "SM");
	   			if (!posGrams.isEmpty()) test = ChiSquareFeatureSelection.chiSquareTestByName(test, posGrams, "POS ");
	   			if (!posbigrams.isEmpty()) test = ChiSquareFeatureSelection.chiSquareTestByName(test, posbigrams, "POS_BIGRAM ");
	   			if (!syntaxposbigrams.isEmpty()) test = ChiSquareFeatureSelection.chiSquareTestByName(test, syntaxposbigrams, "POS_SYNTAX_BIGRAM ");
	   			if (!syntaxbigrams.isEmpty()) test = ChiSquareFeatureSelection.chiSquareTestByName(test, syntaxbigrams, "SYNTAX_BIGRAM ");
	   			if (!bigrams.isEmpty()) test = ChiSquareFeatureSelection.chiSquareTestByName(test, bigrams, "BIGRAM ");
	   			if (!grams.isEmpty()) test = ChiSquareFeatureSelection.chiSquareTestByName(test, grams, "GRAM ");
	   			
	   			if (rangeType.equals("year")) {
	   				try {
	   					test = InstancesProcessor.removeList(test, String.valueOf(test.attribute("AGE").index()+1));
	   				} catch (Exception e) {
	   					e.printStackTrace();
	   				}
	   			}
	   			
	   			this.writeToFile(test,testFile + "-filtered.arff");
	   			System.out.println("Writing to: " + testFile + "-filtered.arff");
	   	   		System.err.println("DONE");
   			} catch (Exception e) {
   				e.printStackTrace();
   			}
   		}
	}
	
	public void addGrams(String arff, String trainArff, int gramAmount, int start, String xtractDirectory, String comparer, boolean balanced, int size, boolean remove) {
		
		//Integer [] gramFeatures = {15,16,18,19};
		//Integer [] gramFeatures = {3,13};
		Integer [] gramFeatures = {FastWeka.POS}; //FastWeka.SYNTAX_BIGRAMS_POS};
		List<Integer> attributesKeep = Arrays.asList(gramFeatures);
		Instances instances = this.loadInstances(arff);
		Instances train = this.loadInstances(trainArff);
		
		System.out.println(train.numAttributes() + " attributes.");
		if (remove) {
			try {
				instances = InstancesProcessor.removeByName(instances, "GRAM "); //"POS_SYNTAX_BIGRAM ");
				train = InstancesProcessor.removeByName(train, "GRAM "); //"POS_SYNTAX_BIGRAM ");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		System.out.println(train.numAttributes() + " attributes.");
		
		System.err.print("[FastWeka.run] (" + new java.text.SimpleDateFormat("hh:mm aaa").format(new Date()) + ") Load training and test set... ");
   		// load training set
   		blogs = loadTrainingSet(this.profileList,size, start, balanced);
		System.err.println("DONE");
		
		// load all attributes - including grams, bigrams, syntax bigrams
		System.err.print("[FastWeka.run] (" + new java.text.SimpleDateFormat("hh:mm aaa").format(new Date()) + ") load attributes... "); 
		
   		ArrayList<Attribute> attributes = loadAttributes(ageRanges, attributesKeep,xtractDirectory, gramAmount, comparer, false);
   		System.err.println("DONE");

   		// create Instances and fill with data
   		System.err.println("[FastWeka.run] (" + new java.text.SimpleDateFormat("hh:mm aaa").format(new Date()) + ") compute instances... "); 
   		instances = addToInstances(attributes, train, instances, blogs);
   		System.err.println("DONE");
   		System.err.println("[FastWeka.run] (" + new java.text.SimpleDateFormat("hh:mm aaa").format(new Date()) + ") write to arff file... "); 
   		this.writeToFile(instances, arff + ".addmissing.arff");
   		System.err.println("DONE");
	}

	public void removeCountry(String arff) {
		
		Instances instances = this.loadInstances(arff);
		
		System.err.print("[FastWeka.run] (" + new java.text.SimpleDateFormat("hh:mm aaa").format(new Date()) + ") Load training set... ");
		blogs = new ArrayList<String>();
   		// load training set
		for (int index = 0; index <=7000; index+=1500) {
			blogs.addAll(loadTrainingSet(entriesDirectory,1500, index, false));
			System.out.println();
		}
		System.err.println("DONE");

   		// remove Instances that are not country US
   		System.err.println("[FastWeka.run] (" + new java.text.SimpleDateFormat("hh:mm aaa").format(new Date()) + ") compute instances... " + instances.numInstances()); 
   	
   		try {
			
			int count = 0;
			
			for (String blog : blogs) {
				//count++;
				if (blogs.size() > 10 && (count % Math.max(10,Math.ceil(blogs.size()/20))) == 0) 
					System.err.println("[FastWeka.getInstances] (" + new java.text.SimpleDateFormat("hh:mm aaa").format(new Date()) + ") Completed " + count + "/" + blogs.size() + "..."); 
				if (Blog.processBlog(profilesDirectory, null, blog).getCountry().matches("UK|CA|AU")) 
					instances.delete(count);
				else count++;
			}		
		} catch (Exception e) {
			System.err.println("[FastWeka.getInstances] " + e);
			e.printStackTrace();
			System.exit(0);
		}
   		
   		System.err.println("DONE");
   		System.err.println("[FastWeka.run] (" + new java.text.SimpleDateFormat("hh:mm aaa").format(new Date()) + ") write to arff file... " + instances.numInstances()); 
   		this.writeToFile(instances, outputDirectory + "/" + rangeType + (year != null ? "-" + year : "") + ".tf.us.arff");
   		System.err.println("DONE");
	}

	
	/**
	 * Use to load data such as dictionary, emoticons, and acronyms.
	 * File must have each element alone on a single line.
	 * @param directory
	 * @return
	 */
	public static HashSet<String> loadDataFile(String directory) {
		HashSet<String> data = new HashSet<String>();
		
		try {
   			BufferedReader in = new BufferedReader(
					new FileReader(directory));
   			
   			String word = "";
   			
   			while ((word = in.readLine()) != null) {
	        	data.add(word.toLowerCase());
   			}
		} catch (Exception e) {
			System.err.println("[FastWeka.loadDataFile] Error processing data file: " + e);
			e.printStackTrace();
		}
		return data;
	}
	
	public void removeblacklisted(String usersFile, String blacklistFile, double threshold) {
		HashSet<String> blacklist = new HashSet<String>();
		
		List<String> b = GeneralUtils.readLines(blacklistFile);
		
		for (String line : b) {
			String [] data = line.split(",");
			
			if (Double.valueOf(data[0]) < threshold) {
				String u = data[1].trim().substring(1).toLowerCase();
				blacklist.add(u); //remove hashtag
			}
		}
		
		List<String> users = GeneralUtils.readLines(usersFile);
		
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(usersFile + ".bl"));
		
			for (String u : users) {
				String [] data = u.split(",");
				try {
					Blog user = Blog.processBlog(this.entriesDirectory + "/" + data[0] + ".xml",0,0,0,0);
					if (blacklist.contains(user.getUsername().toLowerCase())) continue;
					out.write(u + "\n");
				} catch (Exception e) {
					System.out.println("INFO: " + u);
					e.printStackTrace();
					continue;
				}
			}
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private List<String> loadTrainingSet(String file, int size, int start, boolean balanced) {
		
		if (!new File(file).exists()) {
			System.err.println("File to be loaded, " + file + ", does not exist.");
			System.exit(0);
		}
		
		// get list of profiles
		HashSet<String> list = LiveJournalRobot.loadProfileList(file);
   		
		// initialize
   		List<String> blogSet = new ArrayList<String>();
   		Hashtable<String,List<String>> balancedBlogSet = new Hashtable<String,List<String>>();
   		
   		HashMap<String,Integer> counts = new HashMap<String,Integer>(); 
   		
		// go through hash to get correct number of bloggers per age group
		//Iterator<String> it = list.iterator();
		
		int count = 0;
		int index = 0;
		
		//while (it.hasNext() && (size == -1 || balanced || ((ageRanges != null && count / ageRanges.length < size) || count < size))) {
		for (String profile : list) {
			
			if (index < start) {
				index++;
				continue;
			}
			if (count >= size) break;
			
			String [] data = profile.split(",");
        	
			String range = "";
			if (rangeType.equals("year")) range = String.valueOf(this.getRange(Integer.valueOf(data[1])));
			else if (rangeType.equals("gender")) range = data[1];
			else if (rangeType.equals("political_party") || rangeType.equals("political_view") || rangeType.equals("religion"))
				range = data[1];
			else range = String.valueOf(this.getRange(Integer.valueOf(data[2])));
				
			//if (range >= 0 && (size == -1 || balanced || !counts.containsKey(String.valueOf(range)) || counts.get(String.valueOf(range)) < size+start)) {
					
				 counts.put(range, counts.containsKey(range) ? counts.put(range,counts.get(range)+1) : 1);
				//if (!balanced && counts.get(String.valueOf(range)) <= start) continue;
				count++;
				if (balanced) {
					String key = rangeType.equals("year") ? 
							data[1] : data[2];
					if (!balancedBlogSet.containsKey(key)) balancedBlogSet.put(key, new ArrayList<String>());
					balancedBlogSet.get(key).add(data[0]);
				}
				else blogSet.add(data[0]);
			
		}
		
		// now get bloggers spread out over all the years!
		if (balanced) {

			count = 0;
			counts = new HashMap<String,Integer>(); 

			while (blogSet.size() < size*ageRanges.length) {
				Iterator<String> balancedit = balancedBlogSet.keySet().iterator();
				
				while (balancedit.hasNext() && (size == -1 || (count / ageRanges.length) < size)) {
					String key = (String)balancedit.next();
					int range = this.getRange(Integer.valueOf(key));

					if ((size > 0 && counts.get(String.valueOf(range)) >= size) || balancedBlogSet.get(key).size() == 0) continue;
					
					blogSet.add(balancedBlogSet.get(key).remove(0));
					count++;
					 counts.put(String.valueOf(range), counts.containsKey(range) ? counts.put(String.valueOf(range),counts.get(range)+1) : 1);
				}
			}
		}
		
		if (blogSet.size() > 0)
			System.err.print("size:" + blogSet.size() + ", start(" + start + "): " + blogSet.get(0) + ", end(" + (start+size) + "): " + blogSet.get(blogSet.size()-1) + "... ");
		return blogSet;
	}
	
	private List<String> loadTestSet(String profileTestList, int size, int start, boolean balanced) {
   		return this.loadTrainingSet(profileTestList, size, start, balanced);	
	}
	
	public ArrayList<Attribute> loadAttributes(String [] ranges, List<Integer> attributesKeep, String xtractDirectory, int gramAmounts, String comparer, boolean test) {
		ArrayList<Attribute> attributes = new ArrayList<Attribute>();
			
		String [] xtractRanges = new String[ranges == null ? 2 : ranges.length];
		
		if (rangeType.equals("year")) {
			xtractRanges[0] = year + "/L/";
			xtractRanges[1] = year + "/U/";
		}
		else if (rangeType.equals("gender")) {
			xtractRanges[0] = "/F/";
			xtractRanges[1] = "/M/";
		}
		else if (rangeType.equals("political_party")) {
			xtractRanges[0] = "/democrat/";
			xtractRanges[1] = "/republican/";
		}
		else if (rangeType.equals("political_view")) {
			xtractRanges[0] = "/conservative/";
			xtractRanges[1] = "/liberal/";
		}
		else if (rangeType.equals("religion")) {
			xtractRanges = new String[4];
			xtractRanges[0] = "/judaism/";
			xtractRanges[1] = "/islam/";
			xtractRanges[2] = "/christian/";
			//xtractRanges[3] = "/hinduism/";
			xtractRanges[3] = "/atheist/";
			//xtractRanges[5] = "/buddhism/";
		}
		else {
			xtractRanges = ranges;
		}
		
		int window = 10;
		

		for (int attribute : attributesKeep) {
			switch (attribute) {
				case SM:
					// sm
					attributes.add(new Attribute("SM_" + Features.SLANG,attributes.size()));
					attributes.add(new Attribute("SM_" + Features.EMOTICONS,attributes.size()));
					attributes.add(new Attribute("SM_" + Features.ACRONYMS,attributes.size()));
					attributes.add(new Attribute("SM_" + Features.PUNCTUATION,attributes.size()));
					attributes.add(new Attribute("SM_" + Features.LINKS,attributes.size()));
					attributes.add(new Attribute("SM_" + Features.CAPITAL_WORDS,attributes.size()));
					attributes.add(new Attribute(NUM_WORDS,attributes.size()));
					attributes.add(new Attribute("SM_" + Features.PUNCTUATION_NUM,attributes.size()));
					attributes.add(new Attribute("SM_" + Features.EXCLAMATION,attributes.size()));
					attributes.add(new Attribute("SM_" + Features.EXCLAMATION_R,attributes.size()));
					attributes.add(new Attribute("SM_" + Features.PUNCTUATION_R,attributes.size()));
					attributes.add(new Attribute("SM_" + Features.QUESTION,attributes.size()));
					attributes.add(new Attribute("SM_" + Features.QUESTION_R,attributes.size()));
					attributes.add(new Attribute("SM_" + Features.ELLIPSES,attributes.size()));
					attributes.add(new Attribute("SM_" + Features.AVG_WORD_LENGTH,attributes.size()));
					attributes.add(new Attribute("SM_" + Features.ALL_CAPS_WORDS,attributes.size()));
					attributes.add(new Attribute("SM_" + Features.QUOTES,attributes.size()));
					attributes.add(new Attribute("SM_" + Features.WORD_LENGTHENING,attributes.size()));
					break;
				case LIWC:
					addHashToAttributes(attributes,new HashSet<String>(_liwc.getCategories("LIWC ")));
					break;
				case INTERESTS:
					addHashToAttributes(attributes, loadInterests("INTEREST", xtractDirectory, xtractRanges,gramAmounts, window));
					break;
				case FRIENDS:	
					attributes.add(new Attribute("FRIENDS",attributes.size()));
					break;
				case POSTS:
					attributes.add(new Attribute("POSTS",attributes.size()));
					break;
				case GRAMS:	
					addHashToAttributes(attributes,loadGrams("GRAM", xtractDirectory, xtractRanges, "grams", comparer, gramAmounts, window, true));
					break;
				case POS:
					Properties prop = GeneralUtils.getProperties("config/config.properties");
					List<String> posList = GeneralUtils.readLines(prop.getProperty("posList"));
					
					HashSet<String> pos = new HashSet<String>();
					for (String p : posList) {
						pos.add("POS_" + p);
					}
					addHashToAttributes(attributes, pos);
					break;
				case BIGRAMS_POS:
					addHashToAttributes(attributes, loadCollocations("POS_BIGRAM", xtractDirectory, xtractRanges,"bigrams-pos",comparer, gramAmounts, window));
					break;
				case SYNTAX_BIGRAMS_POS:	
					addHashToAttributes(attributes, loadCollocations("POS_SYNTAX_BIGRAM", xtractDirectory,xtractRanges, "syntax-bigrams-pos",comparer, gramAmounts, window));
					break;
				case BIGRAMS:
					addHashToAttributes(attributes, loadCollocations("BIGRAM", xtractDirectory, xtractRanges, "bigrams",comparer, gramAmounts, window));
					break;
				case SYNTAX_BIGRAMS:
					addHashToAttributes(attributes, loadCollocations("SYNTAX_BIGRAM", xtractDirectory, xtractRanges, "syntax-bigrams",comparer, gramAmounts, window));
					break;	
				case COMMENTS:	
					attributes.add(new Attribute("COMMENTS",attributes.size()));
					break;
				case ENTRIES:	
					attributes.add(new Attribute("ENTRIES",attributes.size()));
					break;
				case AVG_TIME:
					attributes.add(new Attribute("AVG_TIME",attributes.size()));
					break;
				case AVG_DAY:	
					attributes.add(new Attribute("AVG_DAY",attributes.size()));
					break;
				case MODE_TIME:
					attributes.add(new Attribute("MODE_TIME",attributes.size()));
					break;
				case MODE_DAY:	
					attributes.add(new Attribute("MODE_DAY",attributes.size()));
					break;	
				case GENDER:
					ArrayList<String> gender = new ArrayList<String>();
					gender.add("M");
					gender.add("F");
					//if (!test) gender.add("U");
					attributes.add(new Attribute("GENDER",gender,attributes.size()));
					break;
				case POLITICAL_VIEW:
					ArrayList<String> view = new ArrayList<String>();
					view.add("conservative");
					view.add("liberal");
					attributes.add(new Attribute("POLITICAL_VIEW",view,attributes.size()));
					break;
				case POLITICAL_PARTY:
					ArrayList<String> party = new ArrayList<String>();
					party.add("republican");
					party.add("democrat");
					attributes.add(new Attribute("POLITICAL_PARTY",party,attributes.size()));
					break;
				case RELIGION:
					ArrayList<String> religion = new ArrayList<String>();
					religion.add("judaism");
					religion.add("islam");
					religion.add("christian");
					//religion.add("hinduism");
					//religion.add("buddhism");
					religion.add("atheist");
					attributes.add(new Attribute("RELIGION",religion,attributes.size()));
					break;	
				case YEAR:
					// put class last
					ArrayList<String> classes = new ArrayList<String>();
					if (ranges != null) {
					    for (String range : ranges)
					    	classes.add(range);
					    attributes.add(new Attribute("YEAR", classes));
					}
					else attributes.add(new Attribute("YEAR",attributes.size()));
			}
		}
		
		return attributes;
	}
		
	private void addHashToAttributes(ArrayList<Attribute> attributes, HashSet<String> set) {
		for (String item : set)
			attributes.add(new Attribute(item,attributes.size()));
	}
	
	private HashSet<String> loadInterests(String type, String directory, String [] xtractRanges, int total, int window) {
		
		HashSet<String> interests = new HashSet<String>();
		List<Hashtable<String,Integer>> rangeInterests = new ArrayList<Hashtable<String,Integer>>();
		
		for (int range = 0; range < xtractRanges.length; range++) {
			String file = directory + "/" + xtractRanges[range] + "/interests.txt";
		
			try {
	   			BufferedReader in = new BufferedReader(
						new FileReader(file));
	   			
	   			int count = 0;
	   			String nextLine;
	   			Hashtable<String,Integer> set = new Hashtable<String,Integer>();
	   			
	   			while((nextLine = in.readLine()) != null && count < total) {
	   				String [] data = nextLine.split(",");
	  				count++;
	  				set.put(type + " " + data[0].trim().toLowerCase(),count);
	  				interests.add(type + " " + data[0].trim().toLowerCase());
	   			}
	   			rangeInterests.add(set);
			} catch (Exception e) {
				System.err.println("[FastWeka.loadGrams] Error processing data file: " + e);
				e.printStackTrace();
			}
		}
		//System.err.println("[FastWeka.loadInterests] (" + new Date() + ") Added " + interests.size() + " interests.");
		
		HashSet<String> pruned = prune(interests,rangeInterests, window);
		
		//System.err.println("[FastWeka.loadInterests] (" + new Date() + ") After pruning " + pruned.size() + " interests.");
		return pruned;
	}	
	
	public HashSet<String> loadGrams( String type, String directory, String [] xtractRanges, String typeName, String comparer, int total, int window, boolean prune) {
		
		HashSet<String> grams = new HashSet<String>();
		List<Hashtable<String,Integer>> rangeGrams = new ArrayList<Hashtable<String,Integer>>();
		
		for (int range = 0; range < xtractRanges.length; range++) {
			String file = directory + "/" + xtractRanges[range] + "/" + typeName + "-" + comparer + ".txt";
		
			try {
	   			BufferedReader in = new BufferedReader(
						new FileReader(file));
	   			
	   			int count = 0;
	   			String nextLine;
	   			Hashtable<String,Integer> set = new Hashtable<String,Integer>();
	   			
	   			while((nextLine = in.readLine()) != null && count < total) {
	   				String [] data = nextLine.split(",");
	   				
	   				String word = data[0];
	   				
	   				if (data.length > 2) {
	   					word = data[data.length-1];
	   				}
	   				
	   				//if (Integer.valueOf(data[1]) < numOccurences) break;
	  				count++;
	   				
	  				set.put(type + " " + word.trim().toLowerCase(),count);
	   				grams.add(type + " " + word.trim().toLowerCase());
	   			}
	   			rangeGrams.add(set);
			} catch (Exception e) {
				System.err.println("[FastWeka.loadGrams] Error processing data file: " + e);
				e.printStackTrace();
			}
		}
		//System.err.println("[FastWeka.loadGrams] (" + new Date() + ") Added " + grams.size() + " grams.");
		HashSet<String> pruned = grams;
		//if (prune) pruned = chiSquare(rangeGrams, 3.84); //prune(gram,rangeGrams,window);
		if (DEBUG) System.err.println("[FastWeka.loadGrams] (" + new Date() + ") After pruning " + pruned.size() + " " + type + " grams.");
		return pruned;
	}	
	
	private HashSet<String> prune(HashSet<String> original, List<Hashtable<String,Integer>> range, int window) {
		
		HashSet<String> pruned = new HashSet<String>();
		
		for (String term : original) {
			
			if (!rangeType.equals("age")) { // || rangeType.equals("gender")) {
				if (!range.get(0).containsKey(term) || !range.get(1).containsKey(term)) {
					/*if (range.get(0).containsKey(term)) 
						System.err.println(term + "," + range.get(0).get(term) + ",NA");
					else 
						System.err.println(term + ",NA," + range.get(1).get(term));*/
					pruned.add(term);
					continue;
				}
				int p1 = range.get(0).get(term);
				int p2 = range.get(1).get(term);
				if (Math.abs(p1-p2) > window) {
					pruned.add(term);
					//System.err.println(term + ", " + p1 + ", " + p2);
				}
			}
			else {
				if (!range.get(0).containsKey(term) || !range.get(1).containsKey(term) || !range.get(2).containsKey(term)) {
					/*if (range.get(0).containsKey(term)) 
						System.err.println(term + "," + range.get(0).get(term) + ",NA,NA");
					else if (range.get(1).containsKey(term))
						System.err.println(term + ",NA," + range.get(1).get(term) + ",NA");
					else 
						System.err.println(term + ",NA,NA," + range.get(2).get(term));*/
					pruned.add(term);
					continue;
				}
				int p1 = range.get(0).get(term);
				int p2 = range.get(1).get(term);
				int p3 = range.get(2).get(term);
				if (Math.abs(p1-p2) <= window || Math.abs(p1-p3) <= window || Math.abs(p2-p3) <= window) continue;
				pruned.add(term);
				//System.err.println(term + ", " + p1 + ", " + p2 + ", " + p3);
			}
		}
		return pruned;
	}
	
	private HashSet<String> loadCollocations(String type, String directory, String [] xtractRanges, String typeName, String comparer, int total, int window) {
		
		List<Hashtable<String,Integer>> rangeCollocations = new ArrayList<Hashtable<String,Integer>> ();
		HashSet<String> collocations = new HashSet<String>();
		
		for (int range = 0; range < xtractRanges.length; range++) {
			String file = directory + "/" + xtractRanges[range] + "/" + typeName + "-" + comparer + ".txt";
			String nextLine = null;
			
			Hashtable<String,Integer> set = new Hashtable<String,Integer>();
			
			try {
	   			BufferedReader in = new BufferedReader(
						new FileReader(file));
	
	   			int index = 0;
	   			
	   			while((nextLine = in.readLine()) != null  && index < total) {
					String [] data = nextLine.split(",");
							
					// so we have the reg exp for search (add if bigram existed in other set)
					String bigram [] = data[data.length-2].toLowerCase().split(" ");
					String key = null;
					
					if (bigrams.containsKey(data[data.length-2].toLowerCase())) {
						if (!bigrams.get(data[data.length-2]).equals(data[data.length-1])) 
							bigrams.put(data[data.length-2].toLowerCase(), bigrams.get(data[data.length-2]) + "|" + data[data.length-1]);
						key = data[data.length-2].toLowerCase();
					}
					else if (bigrams.containsKey((bigram[1] + " " + bigram[0]).toLowerCase())) {
						bigrams.put((bigram[1] + " " + bigram[0]).toLowerCase(), bigrams.get(bigram[1] + " " + bigram[0]) + "|" + data[data.length-1]);
						key = (bigram[1] + " " + bigram[0]).toLowerCase();
					}
					else {
						bigrams.put(data[data.length-2].toLowerCase(), data[data.length-1]);
						key	= data[data.length-2].toLowerCase();
					}
					index++;
					set.put(type + " " + key, index);
					collocations.add(type + " " + key);
	   			}
			} catch (Exception e) {
				if (DEBUG) {
					System.err.println("[FastWeka.loadCollocations] Error processing data file on \"" + nextLine + "\": " + e);
					e.printStackTrace();
				}
			}
			rangeCollocations.add(set);
		}
		
		if (DEBUG) System.err.println("[FastWeka.loadCollocations] (" + new Date() + ") Added " + collocations.size() + " " + typeName + " collocations.");
		HashSet<String> pruned = chiSquare(rangeCollocations, 3.84);// prune(collocations,rangeCollocations, window);
		//System.err.println("[FastWeka.loadCollocations] (" + new Date() + ") After pruning " + pruned.size() + " " + typeName + " collocations.");
		return pruned;
	}
	
	private HashSet<String> chiSquare(List<Hashtable<String,Integer>> grams, double threshold) {
		
		HashSet<String> words = new HashSet<String>();
		
		for (int index = 0; index < grams.size(); index++) {
			Hashtable<String,Integer> c = grams.get(index);
			for (String word : c.keySet()) {
				words.add(word);
			}
		}
		
		return words;
	}
	
	private Instances getInstances(ArrayList<Attribute> attVector, List<String> blogs) {
		
		Instances instances = null;
		String name = "LIVEJOURNAL";
		
		System.out.println("Adding " + blogs.size() + " blogs.");
		
		try {
			instances = new Instances(name, attVector, 0);
			int count = 0;
			
			for (String blog : blogs) {
				count++;
				System.out.println(count + "/" + blogs.size() + ". " + blog);
				if (count % 100 == 0) 
					System.err.println("[FastWeka.getInstances] (" + new java.text.SimpleDateFormat("hh:mm aaa").format(new Date()) + ") Completed " + count + "/" + blogs.size() + "..."); 
				Instance i = getInstance(attVector, blog);
				if (i != null) instances.add(i);
			}		
		} catch (Exception e) {
			System.err.println("[FastWeka.getInstances] " + e);
			e.printStackTrace();
			System.exit(0);
		}
		instances.setClass(instances.attribute(rangeType.toUpperCase()));
		return instances;
	}

	private Instances addToInstances(ArrayList<Attribute> attVector, Instances train, Instances instances, List<String> blogs) {
	
		for (int index = 0; index < attVector.size(); index++) {
			Attribute att = (Attribute)attVector.get(index);
			if (instances.attribute(att.name()) != null) continue;
			if (train.attribute(att.name()) != null) 
				instances.insertAttributeAt(att, train.attribute(att.name()).index());
			else instances.insertAttributeAt(att, instances.numAttributes()-1);
		}
	
		try {
			
			int count = 0;
			for (int index = 0; index < blogs.size(); index++) {
				
				Blog blog = null;
				
				if (profilesDirectory == null) 
					blog = Blog.processBlog(this.entriesDirectory + "/" + blogs.get(index) + ".xml",_max,0,-1,-1);
				else	
					blog = Blog.processBlog(this.profilesDirectory, this.entriesDirectory, blogs.get(index),_max,0,-1,-1);

				if (rangeType.equals("religion") && (blog.getReligion() == null || blog.getReligion().toLowerCase().startsWith("hind") 
						|| blog.getReligion().toLowerCase().startsWith("budd"))) continue;
				if (rangeType.equals("political_party") && blog.getPoliticalParty() == null) continue; 
				if (rangeType.equals("political_view") && blog.getPoliticalView() == null) continue;
				if (blog.getCountry() != null && blog.getCountry().matches("UK|CA|AU")) {
					System.err.println("skip " + blogs.get(index) + ", " + blog.getCountry());
					continue; 
				}
				
				System.out.println(index + ". " + blogs.get(index));
				if (index % 100 == 0) 
					System.err.println("[FastWeka.addToInstances] (" + new java.text.SimpleDateFormat("hh:mm aaa").format(new Date()) + ") Completed " + index + "/" + blogs.size() + "..."); 
				addToInstance(instances, instances.instance(count),attVector, blog);
				count++;
			}		
		} catch (Exception e) {
			System.err.println("[FastWeka.addToInstances] " + e);
			e.printStackTrace();
			System.exit(0);
		}
		return instances;
	}
	
	public Instance addToInstance(Instances instances, Instance instance, ArrayList<Attribute> attributes, Blog blog) {

		String text = getBlogText(blog, false); 
		String pos = getBlogText(blog, true);
		
		double numWords = (double)text.split("\\s+").length;
		
		for (int index = 0; index < attributes.size(); index++) {
				
			Attribute attribute = (Attribute)instance.attribute(instances.attribute(((Attribute)attributes.get(index)).name()).index());
			String name = attribute.name();
			
			if (name.startsWith("INTEREST")) {
				instance.setValue(attribute, blog.hasInterest(name.substring("INTEREST ".length())) ? 1 : 0);
			}
			else if (name.startsWith("BIGRAM")) {
				instance.setValue(attribute, this.searchEntries(text, 
						bigrams.get(name.substring("BIGRAM ".length())), true, _boolean)/numWords);
			}
			else if (name.startsWith("SYNTAX_BIGRAM")) {
				instance.setValue(attribute, this.searchEntries(text, 
						bigrams.get(name.substring("SYNTAX_BIGRAM ".length())), true, _boolean)/numWords);
			}
			else if (name.startsWith("POS_BIGRAM")) {
				instance.setValue(attribute, this.searchEntries(pos, 
						bigrams.get(name.substring("POS_BIGRAM ".length())), true, _boolean)/numWords);
			}
			else if (name.startsWith("POS_SYNTAX_BIGRAM")) {
				instance.setValue(attribute, this.searchEntries(pos,
						bigrams.get(name.substring("POS_SYNTAX_BIGRAM ".length())), true, _boolean)/numWords);
			}
			else if (name.startsWith("GRAM")) {
				instance.setValue(attribute, this.searchEntries(text, name.substring("GRAM ".length()), false, _boolean)/numWords);
			}
			else if (name.startsWith("POS ")) {
				instance.setValue(attribute, this.searchEntries(this.getPOSOnly(blog), name.substring("POS ".length()), false, _boolean)/numWords);
			}
			else if (name.startsWith("LIWC") || name.startsWith("SM")) {
				//instance.setValue(attribute, attributeCount.get(name) / attributeCount.get(NUM_WORDS));
			} 
			else if (name.startsWith("GENDER")) {
				instance.setValue(attribute, blog.getGender() != null ? blog.getGender() : _gender.getGender(blog));
			}
			else {
				System.err.println("[FastWeka.getInstance] invalid attribute: " + name);
			}
		}
		return instance;
	}
	
	private int getRange(int year) {
		
		for (int index = 0; ageRanges != null && index < ageRanges.length; index++) {
			int dash = ageRanges[index].indexOf("-");
			int start = Integer.valueOf(ageRanges[index].substring(0, dash));
			int end = Integer.valueOf(ageRanges[index].substring(dash + 1));
			
			if (year >= start && year <= end) return index;
		}
		return year;
	}
	
	public int searchEntries(String text, String query, boolean regex) {
		return searchEntries(text, query, regex, false);
	}
	
	public int searchEntries(String text, String query, boolean regex, boolean findOnce) {
			int count = 0;
			
			if (query == null) {
				System.out.println("Missing query");
				return 0;
			}
			
			try {
				if (!regex) {
					String tmp = "\\Q" + query + "\\E"; 
					query = tmp;
				}
				
				// [\s^]\b\Qnnps\E[\s$]
				Pattern p = Pattern.compile("[\\s^](" + query + ")[\\s$]",Pattern.CASE_INSENSITIVE);
				Matcher m = p.matcher(text);
				
				while (m.find()) {
					if (findOnce) return 1;
					count++;
				}

			} catch (Exception e) {
				System.err.println("[FastWeka.searchEntries] Error occurred in finding match for \"" 
						+ query + "\" in entry " + text + "\n: " + e);
				e.printStackTrace();
			}
		return count;
	}
	
	public String getBlogText(Blog blog, boolean pos) {
		String text = "";
		Iterator<String> it = blog.getEntries();
		
		int i = 0;
		
		while (it.hasNext()) {
			if (i > _max) continue;
			blog.threaded.livejournal.Entry et = (blog.threaded.livejournal.Entry)blog.getEntry((String)it.next());
			text += pos ? getTruncatedPOS(et.getEntry().toLowerCase().replaceAll("_", "/")) : et.getEntryText().toLowerCase() + " ";
			i++;
		}
		text = convertLinks(text, _twitter);
		return text;
	}
	
	public String getPOSOnly(Blog blog) {
		String text = "";
		Iterator<String> it = blog.getEntries();
		
		int i = 0;
		
		while (it.hasNext()) {
			if (i > _max) continue;
			blog.threaded.livejournal.Entry et = (blog.threaded.livejournal.Entry)blog.getEntry((String)it.next());
			text += et.getPOSText().toLowerCase();
			i++;
		}
		return text;
	}
	
	public String getTruncatedPOS(String text) {
		text = text.replaceAll("/vb*\b", "/vb");
		text = text.replaceAll("/nn*\b", "/nn");
		text = text.replaceAll("/[pw]?dt\b", "/dt");
		text = text.replaceAll("/[w]?rb[rs]?", "/rb");
		text = text.replaceAll("wp[\\$]?", "prp");
		text = text.replaceAll("prp[\\$]?", "prp");
		text = text.replaceAll("jj[rs]?", "jj");
		return text;
	}
	
	public static Hashtable<String,Double> computeSMFrequencyStats(Blog blog, LIWC liwc, Features smFeatures, boolean twitter, int max) {
		Iterator<String> it = blog.getEntries();
		
		String text = "";
		double comments = 0;
		int [] days = new int[7];
		int [] times = new int[24];
		
		int i = 0;
		
		while (it.hasNext()) {
			if (i > max) continue;
			blog.threaded.livejournal.Entry entry = (blog.threaded.livejournal.Entry)blog.getEntry((String)it.next());
			
			comments += (double)entry.getNumComments();
			
			if (entry.getDate() != null) {
				Calendar date = Calendar.getInstance();
				date.setTime(entry.getDate());
				
				if (date.get(Calendar.DAY_OF_WEEK) > 0)
					days[date.get(Calendar.DAY_OF_WEEK)-1]++;
				if (date.get(Calendar.HOUR_OF_DAY) > 0) 
					times[date.get(Calendar.HOUR_OF_DAY)-1]++;
			}
			List<blog.Sentence> sentences = entry.getSentences();
			
			for (blog.Sentence sentence : sentences) {
				
				if (sentence == null) continue;
				text += sentence.getText() + " ";
			}
			i++;
		}
		Hashtable<String,Double> sm = smFeatures.compute(convertLinks(text, twitter).split("\\s+"));
		Hashtable<String,Double> stats = new Hashtable<String,Double>();
		for (String stat : sm.keySet()) {
			stats.put("SM_" + stat, sm.get(stat));
		}
		
		if (liwc != null) stats.putAll(new Hashtable<String,Double>(liwc.generateFeatureCounts(text, "LIWC ")));
		stats.put("COMMENTS", comments / blog.getNumEntries());
		stats.put("MODE_TIME",(double)getMode(times));
		stats.put("MODE_DAY",(double)getMode(days));
		return stats;
	} 
	
	public static String convertLinks(String text, boolean twitter) {
		text = text.replaceAll("http:", "LLLINKKK ");
		
		// remove twitter @ and #
		if (twitter) {
			text = text.replaceAll("@\\w+\\b+", "UUUSERRR"); //@user
			text = text.replaceAll("#\\w+\\b+", "HHHASHTAGGG"); // #hashtag
			text = text.replaceAll("\\b+rt\\b+", ""); // Retweet
		}
		return text;
	}
		
	public static int getMode(int [] array) {
		int max = array[0];
		int index = 0;
		
		for (int i = 1; i < array.length; i++) {
			if (array[i] > max) {
				max = array[i];
				index = i;
			}
		}	
		return index;
	}
	
	public Instance getInstance(ArrayList<Attribute> attributes, String username) {
		
		Instance instance;
		instance = new DenseInstance(attributes.size());
		
		Blog blog = null;
		if (profilesDirectory == null) 
			blog = Blog.processBlog(this.entriesDirectory + "/" + username + ".xml",_max,0,-1,-1);
		else	
			blog = Blog.processBlog(this.profilesDirectory, this.entriesDirectory, username,_max,0,-1,-1);
		
		if (rangeType.equals("political_party") && blog.getPoliticalParty() == null) return null; 
		if (rangeType.equals("political_view") && blog.getPoliticalView() == null) return null;
		if (rangeType.equals("religion") && (blog.getReligion() == null || blog.getReligion().toLowerCase().startsWith("hind") 
				|| blog.getReligion().toLowerCase().startsWith("budd"))) return null;

		if (blog.getCountry() != null && blog.getCountry().matches("UK|CA|AU")) {
			System.err.println("skip " + username + ", " + blog.getCountry());
			return null; 
		}
		//System.err.println(blog.getCountry());
		
		//Hashtable<String,Double> attributeCount = this.computeTermFrequencyStats(blog);
		Hashtable<String,Double> attributeCount = computeSMFrequencyStats(blog, _liwc, _smFeatures, _twitter, _max);
		
		
		String text = getBlogText(blog, false); 
		String pos = getBlogText(blog, true);
		
		for (int index = 0; index < attributes.size(); index++) {
				
			Attribute attribute = (Attribute)attributes.get(index);
			String name = attribute.name();
			
			if (name.startsWith("YEAR")) {
				if (blog.getLastBuildDate() == null || blog.getDOB() == null) {
					System.err.println("Invalid dob/build: " + username);
					//return null;
					continue;
				}
				int year = Integer.valueOf(new java.text.SimpleDateFormat("yyyy").format(blog.getDOB()));
				int lbd = rangeType.equals("age") ? Integer.valueOf(new java.text.SimpleDateFormat("yyyy").format(blog.getLastBuildDate())) : 0;
				instance.setValue(attribute, this.getRange(Integer.valueOf(
						rangeType.equals("age") ? lbd - year : year)));
			}
			else if (name.startsWith("GENDER")) {
				if (blog.getGender() == null)
					if (_gender.getGender(blog).equals("U")) return null;
				instance.setValue(attribute, blog.getGender() != null ? blog.getGender() : _gender.getGender(blog));
			}
			else if (name.startsWith("RELIGION")) {
				System.out.println(blog.getReligion().trim().replaceAll("\"", ""));
				instance.setValue(attribute, blog.getReligion().trim().replaceAll("\"", ""));
			}
			else if (name.startsWith("POLITICAL_PARTY")) {
				instance.setValue(attribute, blog.getPoliticalParty());
			}
			else if (name.startsWith("POLITICAL_VIEW")) {
				instance.setValue(attribute, blog.getPoliticalView());
			}
			else if (name.startsWith("INTEREST")) {
				instance.setValue(attribute, blog.hasInterest(name.substring("INTEREST ".length())) ? 1 : 0);
			}
			else if (name.startsWith("BIGRAM")) {
				instance.setValue(attribute, this.searchEntries(text, 
						bigrams.get(name.substring("BIGRAM ".length())), true, _boolean)/attributeCount.get(NUM_WORDS));
			}
			else if (name.startsWith("SYNTAX_BIGRAM")) {
				instance.setValue(attribute, this.searchEntries(text, 
						bigrams.get(name.substring("SYNTAX_BIGRAM ".length())), true, _boolean)/attributeCount.get(NUM_WORDS));
			}
			else if (name.startsWith("POS_BIGRAM")) {
				instance.setValue(attribute, this.searchEntries(pos, 
						bigrams.get(name.substring("POS_BIGRAM ".length())), true, _boolean)/attributeCount.get(NUM_WORDS));
			}
			else if (name.startsWith("POS_SYNTAX_BIGRAM")) {
				instance.setValue(attribute, this.searchEntries(pos,
						bigrams.get(name.substring("POS_SYNTAX_BIGRAM ".length())), true, _boolean)/attributeCount.get(NUM_WORDS));
			}
			else if (name.startsWith("POS")) {
				instance.setValue(attribute, this.searchEntries(this.getPOSOnly(blog), 
						name.substring("POS ".length()), false, _boolean)/attributeCount.get(NUM_WORDS));
			}
			else if (name.startsWith("GRAM")) {
				instance.setValue(attribute, this.searchEntries(text, 
						name.substring("GRAM ".length()), false, _boolean)/attributeCount.get(NUM_WORDS));
			}
			else if (name.matches("FRIENDS")) {
				instance.setValue(attribute, blog.getFriendCount());
			}
			else if (name.matches("POSTS")) {
				instance.setValue(attribute, blog.getLifetimePostCount());
			}
			// one of the attributes that we computed counts for (eg ACRONYM, COMMENTS, etc..)
			else if (attributeCount.containsKey(name)) {
				instance.setValue(attribute, attributeCount.get(name) / attributeCount.get(NUM_WORDS));
			}
			else {
				System.err.println("[FastWeka.getInstance] invalid attribute: " + name);
			}
		}
		return instance;
	}
	
	public void writeToFile(Instances instances, String outputFile) {
		try {
			 ArffSaver saver = new ArffSaver();
			 saver.setInstances(instances);
			 saver.setFile(new File(outputFile));
			 saver.writeBatch();
		} catch (Exception e) {
			System.err.println("[FastWeka.writeToFile]" + e);
			e.printStackTrace();
		}
	}
	
	public void printStats() {
		
		try {
			Properties prop = GeneralUtils.getProperties("config/config.properties");
			String emoticonFile = prop.getProperty("emoticons_directory") + "/emoticons.txt.codes";
			String acronymFile = prop.getProperty("emoticons_directory") + "/acronyms.txt";
			String dictionaryFile = prop.getProperty("dictionary");
			
			emoticons = new HashSet<String>(processing.SwapEmoticons.readCodes(new File(emoticonFile), false).values());
			acronyms = loadDataFile(acronymFile);
			dictionary  = loadDataFile(dictionaryFile);
		} catch (Exception e) {
			System.err.println("[FastWeka.run] " + e);
   			e.printStackTrace();
		}
		
		// read in profile_list.txt.age
		HashSet<String> list = LiveJournalRobot.loadProfileList(profilesDirectory + "uk-ca-us-au/profile_list.txt.age.all");
   		
		// go through hash to get correct number of bloggers per age group
		Iterator<String> it = list.iterator();
		
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(outputDirectory + "/country.txt"));
			out.write("username,country\n");
			 
			while (it.hasNext()) {
				String input = (String)it.next();
				
				String [] data = input.split(",");			
				Blog blog = Blog.processBlog(this.profilesDirectory, null, data[0]);
				out.write(blog.getUsername() + ", " + blog.getCountry() + "\n");
				
			}
			out.close();
		} catch (Exception e) {
			System.err.println("[FastWeka.printStats] " + e);
			e.printStackTrace();
		}
	}
	
	/**
	 * Create an arff file to be used in author trait detection. 
	 * An arff file can be created for age, gender, religion, and political party 
	 * Steps:
	 * 1. "run" action (can be broken up to be generated faster)
	 * 2. if broken up, files must be concatenated
	 * 3. "filter" action to filter n-gram features
	 * @param args
	 */
	public static void main(String[] args) {

		String [] ages = null;
		String rangeType = "year";
		String experimentName = "";
		int numBloggers = 20;
		int start = 20;
		int numFeatures = 1500;
		String comparer = "ef";
		String year = "1982";
		String action = "add";
		boolean balanced = false;
		String source = "twitter";
		boolean onlineBehavior = false;
		boolean interests = false;
		boolean lexicalStyle =  false;
		boolean collocations = false;
		boolean pos = false;
		boolean ngrams = false;
		boolean liwc = false;
		String directory = ""; ///proj/nlp/users/sara/demographics/";
		String trainArff = "";
		String testArff = "";
		
		System.err.println("START: " + new Date());
		
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-action")) {
				action = args[++i];
			}
			if (args[i].equals("-ages")) {
				ages = args[++i].split(",");
			}
			else if (args[i].equals("-name")) {
				experimentName = args[++i] + "-" + experimentName; 
			}
			else if (args[i].equals("-nb")) {
				numBloggers = Integer.valueOf(args[++i]);
			}
			else if (args[i].equals("-nf")) {
				numFeatures = Integer.valueOf(args[++i]);
				experimentName += numFeatures + "-";
			}
			else if (args[i].equals("-r")) {
				rangeType = args[++i];
				if (rangeType.equals("politics")) rangeType = "political_party";
			}
			else if (args[i].equals("-frequencyType")) {
				comparer = args[++i];
			}
			else if (args[i].equals("-s")) {
				start = Integer.valueOf(args[++i]);
			}
			else if (args[i].equals("-year")) {
				year = args[++i];
				int y = Integer.valueOf(year);
				ages = new String[2];
				ages[0] = "1940-" + (y-1);
				ages[1] = y + "-2000";
			}
			else if (args[i].equals("-source")) {
				source = args[++i];
			}
			else if (args[i].equals("-online")) {
				onlineBehavior = true;
			}
			else if (args[i].equals("-interests")) {
				interests = true;
			}
			else if (args[i].equals("-sm")) {
				lexicalStyle = true;
			}
			else if (args[i].equals("-collocations")) {
				collocations = true;
			}
			else if (args[i].equals("-pos")) {
				pos = true;
			}
			else if (args[i].equals("-ngrams")) {
				ngrams = true;
			}
			else if (args[i].equals("-liwc")) {
				liwc = true;
			}
			else if (args[i].equals("-balanced")) {
				balanced = true;
			}		
			else if (args[i].equals("-o")) {
				directory = args[++i];
			}
			else if (args[i].equals("-trainArff")) {
				trainArff = args[++i];
			}
			else if (args[i].equals("-testArff")) {
				testArff = args[++i];
			}
		}
		
		experimentName = rangeType + "-" + comparer + "-" + start;
		System.err.println("SETTINGS: ");
		System.err.println("name: " + experimentName);
		System.err.print("ages: ");
		if (ages == null) {
			year = "1981";
			System.err.println("All");
		}
		else { 
			for (int i = 0; i < ages.length; i++) {
				System.err.print(ages[i] + " ");
			}
		}
		System.err.println("num bloggers: " + numBloggers);
		System.err.println("num features: " + numFeatures);
		
		List<Integer> attributes = new ArrayList<Integer>();
		
		if (ngrams) {
			attributes.add(FastWeka.GRAMS);
			attributes.add(FastWeka.BIGRAMS);
		}
		if (interests) {
			attributes.add(FastWeka.INTERESTS);
		}
		if (pos) {
			attributes.add(FastWeka.POS);
			attributes.add(FastWeka.BIGRAMS_POS);
		}
		if (onlineBehavior) {
			attributes.add(FastWeka.COMMENTS);
			attributes.add(FastWeka.POSTS);
			attributes.add(FastWeka.ENTRIES);
			attributes.add(FastWeka.FRIENDS);
			attributes.add(FastWeka.MODE_DAY);
			attributes.add(FastWeka.MODE_TIME);
		}
		if (lexicalStyle) {
			attributes.add(FastWeka.SM);
		}
		if (liwc) {
			attributes.add(FastWeka.LIWC);
		}
		if (collocations) {
			attributes.add(FastWeka.SYNTAX_BIGRAMS);
		}
		if (collocations && pos) {
			attributes.add(FastWeka.SYNTAX_BIGRAMS_POS);
		}
		// class feature
		attributes.add(valueOf(rangeType.toUpperCase()));
		
		String profileList = null;
		String profileTestList = null;
		String profilesDirectory = null;
		String processeddirectory = "";
		
		Properties prop = GeneralUtils.getProperties("config/config.properties");
		
		if (rangeType.equals("gender")) {
			// authorship gender
			if (source.equals("authorship")) {
				processeddirectory = prop.getProperty("processeddirectory_gender_authorship");
				profileList = prop.getProperty("profileList_gender_authorship"); 
				profileTestList = prop.getProperty("profileTestList_gender_authorship");
			}
			// gender
			if (source.equals("lj")) {
				processeddirectory = prop.getProperty("profilesdirectory_lj");
				profileList = prop.getProperty("profileList_gender_lj");
				profileTestList = prop.getProperty("profileTestList_gender_lj");  
				profilesDirectory = prop.getProperty("LJdirectory");
			}
		}
		if (rangeType.equals("year")) {
			// authorship year
			if (source.equals("authorship")) {
				processeddirectory = prop.getProperty("processeddirectory_age_authorship");
				profileList = prop.getProperty("profileList_age_authorship"); 
				profileTestList = prop.getProperty("profileTestList_age_authorship"); 
			}
			// year
			if (source.equals("lj")) {
				processeddirectory = prop.getProperty("profilesdirectory_lj");
				profileList = prop.getProperty("profileList_age_lj");
				profileTestList = prop.getProperty("profileTestList_age_lj");  
				profilesDirectory = prop.getProperty("LJdirectory");
			}
		}
		
		// politics
		if (rangeType.startsWith("political_party")) {
			processeddirectory = prop.getProperty("processeddirectory_politics_twitter");
			profileList = prop.getProperty("profileList_politics_twitter"); 
			profileTestList = prop.getProperty("profileTestList_politics_twitter");
		}
		// religion
		else if (rangeType.equals("religion")) {
			processeddirectory = prop.getProperty("processeddirectory_religion_twitter");
			profileList = prop.getProperty("profileList_religion_twitter"); 
			profileTestList = prop.getProperty("profileTestList_religion_twitter");
		}
		
		FastWeka weka = new FastWeka(directory + "/output/", profilesDirectory, processeddirectory, 
				ages, rangeType, year, profileList, profileTestList, source);

		// print stats
		if (action.equals("stats"))
			weka.printStats();
		// run system to create arff file
		else if (action.equals("run"))
			weka.run(attributes, numFeatures, numBloggers, prop.getProperty("xtract") + rangeType, comparer, start, balanced);
		// filter the n-grams after the arff files have been created
		else if (action.equals("filter"))
			weka.loadAndFilter(trainArff , testArff);
		// needed if there are some people that should be ignored within a blacklist 
		else if (action.equals("blacklist")) {
			double threshold = .4;
			//weka.removeblacklisted(profileList,directory + "/blacklist.txt",threshold);
			weka.removeblacklisted(profileTestList,directory + "/blacklist.txt",threshold);
		}
		// add n-gram features later on. shouldn't be needed in general
		/*else if (action.equals("add")) {
			boolean remove = false;
			//String file = "/proj/nlp/users/sara/demographics/output/train/" + rangeType + "/" + (source == "twitter" ? "" : source + "/") 
			//		+ rangeType + "-1982.ef." + start + "-" + (start+numBloggers) + (source == "twitter" ? "" : "." + source) + ".arff";
			String fileTrain = "/proj/nlp/users/sara/demographics/output/train/year/lj/year.lj.arff";
			String fileTest = "/proj/nlp/users/sara/demographics/output/test/year/lj/year.lj.arff";
			System.out.println("Adding to " + fileTest);
			weka.addGrams(fileTest, fileTrain, numFeatures, start, directory + "/xtract-2014/" + rangeType, comparer, balanced, numBloggers, remove);
		}
		// remove country from features. shouldn't be needed in general
		else if (action.equals("remove"))
			weka.removeCountry(directory + "/output/test/" + rangeType + ".arff");
		*/
		// reduce the number of n-grams to be kept. shouldn't be needed in general
		//else if (action.equals("reduce"))
		//	weka.reduceGrams(directory + "/output/train/gender/good/gender.authorship.arff-filtered.arff" , directory + "/output/test/gender/gender.authorship.arff-filtered.arff", directory + "/xtract-2014/" + rangeType, numFeatures);
		System.err.println("END: " + new Date());
	}
}
