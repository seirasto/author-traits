package edu.columbia.demographics.religion;

import java.io.*;
import java.util.*;

import processing.GeneralUtils;

import weka.core.*;
import weka.core.converters.ConverterUtils.DataSource;
import edu.columbia.demographics.DemographicDetector;
import edu.columbia.utilities.weka.InstancesProcessor;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;

public class ReligionDetector extends DemographicDetector {

	String [] _types = {"judaism","islam","christian","atheist"}; //,"buddhism","hinduism"};
	String _type;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		String type = "religion";
		boolean overwrite = false;
		
		try {
			Properties prop = GeneralUtils.getProperties("config/config.properties");
			String modelDirectory = prop.getProperty("model");
			String tagger = prop.getProperty("tagger");

			String trainArff = modelDirectory + type + ".arff"; // "/train/" + type + "/" + type + ".arff-filtered.arff";
			String testArff = modelDirectory + type + ".arff"; // "/test/" + type +  "/" + type + ".arff-filtered.arff";
		
			String iFile = args[1];
					//e.g. "/proj/nlp/fluke/SCIL/corpora/wikipedia/WD_batch2_year3LJ/World_War_II__Archive_46___Obvious_omission.xml";
					//e.g. "/proj/nlp/fluke/SCIL/corpora/livejournal/agreement-batch1/100417.xml";
			String oFile = args[2];
					//e.g. "/proj/nlp/users/sara/influence/processed/processed-World_War_II__Archive_46___Obvious_omission.xml/World_War_II__Archive_46___Obvious_omission.xml";
					//e.g. "/proj/nlp/users/sara/influence/processed/processed-100417.xml/100417.xml.dep.pos";
			
			String xtract = prop.getProperty("xtract");
			
			ReligionDetector religionD = new ReligionDetector(new MaxentTagger(tagger), type, LexicalizedParser.loadModel(),xtract,true);
			String cls = "svm";
			
			String command = args[0];
			if (command.equals("-experiment")) {
				religionD.runExperiments(trainArff, testArff, "RELIGION", cls);
			}
			else if (command.equals("-model")) {

				if (!new File(modelDirectory + "/train/" + cls + "-" + type + ".model").exists() || overwrite) 
					religionD.generateModel(trainArff,cls,modelDirectory + "/");
				religionD.loadClassifier(modelDirectory + "/" + cls + "-" + type + ".model",trainArff);
				religionD.classify(cls, trainArff,testArff);
			}
			else if (command.equals("-run")) {
				
				if (!new File(modelDirectory + "/" + cls + "-" + type + ".model").exists()) {
					System.out.println("Please run model command. Model does not exist at " + modelDirectory + "/" + cls + "-" + type + ".model");
					System.exit(0);
				}
				religionD.loadClassifier(modelDirectory + "/" + cls + "-" + type + ".model",trainArff);
				religionD.run(iFile, oFile, cls);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public ReligionDetector(MaxentTagger tagger, String type, LexicalizedParser parser, String xtract, boolean twitter) {
		super(tagger, parser, type, new Integer [] {0,3,5,9,10,11,12,13,15,16,17,18,19,21,25},xtract, twitter);
		_type = type;
	}
	
	public void classify(String cls, String trainArff, String testArff) {
		Instances train = update(removeIrrelevantAttributes(loadInstances(trainArff)));
		System.out.println("Class Breakdown Training: " + Arrays.toString(train.attributeStats(train.classIndex()).nominalCounts));
		Instances test = update(removeIrrelevantAttributes(loadInstances(testArff)));
		System.out.println("Class Breakdown Testing: " + Arrays.toString(test.attributeStats(test.classIndex()).nominalCounts));
		this.classify(_classifier, cls, train, test);
	}

	public void generateModel(String arff, String cls, String output) {
		try {
			Instances instances = DataSource.read(arff); //loadInstances(arffDir);
			System.out.println("There are " + instances.numInstances() + " instances.");
			
			// remove irrelevant attributes and instances
			instances = removeIrrelevantAttributes(instances);
			instances = update(instances);
			System.out.println("There are " + instances.numInstances() + " instances.");
			generateModel(instances,cls,output,_type.toUpperCase());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	protected Instances update(Instances instances) {
		instances.setClass(instances.attribute(_type.toUpperCase()));
		instances = removeMissingClasses(instances);
		return instances;
	}
	
	protected Instances removeIrrelevantAttributes(Instances instances) {
		removeIrrelevantDemographicAttributes(instances);
		
		String remove = "";
		
		for (int index = 0; index < instances.numAttributes(); index++) {
			if (instances.attribute(index).name().startsWith("POS ")) 
				remove += (index+1) + ",";
		}
		
		try {
			instances = InstancesProcessor.removeList(instances,remove);
		//instances.deleteAttributeAt(instances.attribute("YEAR").index());
		} catch(Exception e) {
			e.printStackTrace();
		}
		return instances;
	}	
	
	protected Instances removeMissingClasses(Instances instances) {
		
		HashSet<String> types = new HashSet<String>(Arrays.asList(_types));
		
		for (int index = 0; index < instances.numInstances(); index++) {
			if (!types.contains(instances.attribute(instances.classIndex()).value((int)instances.instance(index).classValue()))) {
				instances.delete(index);
				index--;
			}
		}

		instances.renameAttribute(instances.attribute("RELIGION"),"RELIGION3");	
		//Attribute gender3 = instances.attribute("GENDER3");	
		//Attribute gender = newInstances.attribute("GENDER");
		//newInstances.deleteAttributeAt(gender.index());
		Instances newInstances =  new Instances(instances,0);
		
		ArrayList<String> g = new ArrayList<String>();
		
		for (String type : _types)
			g.add(type);
		
		Attribute religion = new Attribute("RELIGION",g);
		newInstances.insertAttributeAt(religion,newInstances.numAttributes()-1);
		instances.insertAttributeAt(religion,instances.numAttributes()-1);
		
		for (int i = 0; i < instances.numInstances(); i++) {
			Instance instance = instances.instance(i);
			instance.setValue(instances.attribute("RELIGION"),instance.stringValue(instances.attribute("RELIGION3")));
			newInstances.add(instance);
		}
		newInstances.setClass(instances.attribute("RELIGION"));
		newInstances.deleteAttributeAt(newInstances.attribute("RELIGION3").index());
		return newInstances;
	}
	
	public void run(String iFile, String oFile, String cls, String model, String arff) {
		
		try {
			// load training instances
			Instances train = DataSource.read(arff); //loadInstances(arff);
			_train = removeIrrelevantAttributes(train);
			_train = update(train);
			
			run(iFile, oFile, cls);
			} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

	}
}
