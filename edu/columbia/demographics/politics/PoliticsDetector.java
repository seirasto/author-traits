package edu.columbia.demographics.politics;

import java.io.*;
import java.util.Arrays;
import java.util.Properties;

import processing.GeneralUtils;

import weka.core.*;
import weka.core.converters.ConverterUtils.DataSource;
import edu.columbia.demographics.DemographicDetector;
//import edu.columbia.utilities.weka.InstancesProcessor;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;

public class PoliticsDetector extends DemographicDetector {

	String [] _politicalparty = {"republican","democrat"};
	String [] _politicalview = {"conservative","liberal"};
	String [] _types;
	String _type;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		String type = "political_party";
		boolean overwrite = false;
		String command = args[0];
		
		try {
			Properties prop = GeneralUtils.getProperties("config/config.properties");
			String modelDirectory = prop.getProperty("model");
			String tagger = prop.getProperty("tagger");

			String trainArff = modelDirectory + "/" + type + ".arff";
			String testArff = modelDirectory + "/" + type + ".arff";
			
			String iFile = args[1];
					// e.g. "/proj/nlp/fluke/SCIL/corpora/wikipedia/WD_batch2_year3LJ/World_War_II__Archive_46___Obvious_omission.xml";
					// e.g. "/proj/nlp/fluke/SCIL/corpora/livejournal/agreement-batch1/100417.xml;
			String oFile = args[2];
					// e.g. "/proj/nlp/users/sara/influence/processed/wikipedia/processed-World_War_II__Archive_46___Obvious_omission.xml/ World_War_II__Archive_46___Obvious_omission.xml";
					// e.g. "/proj/nlp/users/sara/influence/processed/processed-100417.xml/100417.xml.dep.pos";
			
			String xtract = prop.getProperty("xtract");
			
			PoliticsDetector politicD = new PoliticsDetector(new MaxentTagger(tagger), type, LexicalizedParser.loadModel(),xtract,true);
			String cls = "svm";
			
			if (command.equals("-experiment")) {
				politicD.runExperiments(trainArff, testArff, type.toUpperCase(), cls);
			}
			else if (command.equals("-model")){
				if (overwrite || !new File(modelDirectory + "/" + cls + "-" + type + ".model").exists()) 
					politicD.generateModel(trainArff,cls,modelDirectory + "/train/");
				politicD.loadClassifier(modelDirectory + "/" + cls + "-" + type + ".model",trainArff);
				politicD.classify(cls, trainArff,testArff);
			}
			else if (command.equals("-run")) {
				if (!new File(modelDirectory + "/" + cls + "-" + type + ".model").exists()) {
					System.out.println("Please run model command. Model does not exist at " + modelDirectory + "/" + cls + "-" + type + ".model");
					System.exit(0);
				}	
				politicD.loadClassifier(modelDirectory + "/" + cls + "-" + type + ".model",trainArff);
				politicD.run(iFile, oFile, cls);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public PoliticsDetector(MaxentTagger tagger, String type, LexicalizedParser parser, String xtract, boolean twitter) {
		super(tagger, parser, type, new Integer [] {0,3,5,9,10,11,12,13,15,16,17,18,19,21},xtract, twitter);
		if (type.equals("political_party")) _types = _politicalparty;
		else _types = _politicalview;
		_type = type;
	}
	
	public void classify(String cls, String trainArff, String testArff) {
		classify(cls, trainArff, testArff, false);
	}
	
	public void classify(String cls, String trainArff, String testArff, boolean binary) {
		Instances train = update(removeIrrelevantAttributes(loadInstances(trainArff)));
		System.out.println("Class Breakdown Training: " + Arrays.toString(train.attributeStats(train.classIndex()).nominalCounts));
		Instances test = update(removeIrrelevantAttributes(loadInstances(testArff)));
		System.out.println("Class Breakdown Testing: " + Arrays.toString(test.attributeStats(test.classIndex()).nominalCounts));
		this.classify(_classifier, cls, train, test, binary);
	}

	public void generateModel(String arff, String cls, String output) {
		try {
			Instances instances = DataSource.read(arff); //loadInstances(arffDir);
			System.out.println("There are " + instances.numInstances() + " instances.");
			
			// remove irrelevant attributes and instances
			instances = removeIrrelevantAttributes(instances);
			instances = update(instances);
			System.out.println("There are " + instances.numInstances() + " instances.");
			generateModel(instances,cls,output,_type.toUpperCase());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	protected Instances update(Instances instances) {
		instances.setClass(instances.attribute(_type.toUpperCase()));
		return instances;
	}
	
	protected Instances removeIrrelevantAttributes(Instances instances) {
		removeIrrelevantDemographicAttributes(instances);
		
		/*String remove = "";
		
		for (int index = 0; index < instances.numAttributes(); index++) {
			if (instances.attribute(index).name().startsWith("POS ")) 
				remove += (index+1) + ",";
		}
		
		try {
			instances = InstancesProcessor.removeList(instances,remove);
		//instances.deleteAttributeAt(instances.attribute("YEAR").index());
		} catch(Exception e) {
			e.printStackTrace();
		}*/
		return instances;
	}	
	
	public void run(String iFile, String oFile, String cls, String model, String arff) {
		
		try {
			// load training instances
			Instances train = DataSource.read(arff); //loadInstances(arff);
			_train = removeIrrelevantAttributes(train);
			_train = update(train);
			
			run(iFile, oFile, cls);
			} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

	}
}
