/**
 * 
 */
package edu.columbia.demographics.gender;

import java.util.*;
import java.io.*;

import processing.GeneralUtils;

import blog.threaded.livejournal.Blog;

/**
 * @author sara
 *
 */
public class Label {

	// 1. Read in name lists
	// 2. Label each blog using name lists
	
	HashMap<String,Integer> _male; 
	HashMap<String,Integer> _female; 
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Label label = new Label();
		//args[0] = "/proj/nlp/users/sara/corpora/blogs/livejournal/2010-corpus/profile_list.txt"
		//args[1] = "/proj/nlp/users/sara/corpora/blogs/livejournal/2010-corpus/"
		label.labelGender(args[0],args[1]);
	}
	
	public Label() {
   		Properties prop = GeneralUtils.getProperties("config/config.properties");
		if (prop.containsKey("ssn_names") && new File(prop.getProperty("ssn_names")).exists()) 
			loadNames(prop.getProperty("ssn_names"), 1950, 2014);
	}
	
	public void loadNames(String directory, int start, int end) {
		_male = new HashMap<String,Integer>();
		_female = new HashMap<String,Integer>();
		
		File [] files = new File(directory).listFiles();
		
		for (File file : files) {
			if (!file.toString().endsWith(".txt")) continue;
			
			int year = Integer.valueOf(file.getName().toString().substring(3,7));
			
			if (year > start && year < end) {
				List<String> lines = processing.GeneralUtils.readLines(file.toString());
				
				for (String line : lines) {
					String [] data = line.split(",");
					if (data[1].equals("F")) {
						_female.put(data[0],_female.containsKey(data[0]) ? _female.get(data[0]) + Integer.valueOf(data[2]) : Integer.valueOf(data[2]));
					}
					else _male.put(data[0],_male.containsKey(data[0]) ? _male.get(data[0]) + Integer.valueOf(data[2]) : Integer.valueOf(data[2]));
				}
			}
		}
	}
	
	public String getGender(Blog blog) {
		if (blog.getFullName() != null) {
			String [] names = blog.getFullName().split("\\s+");
			if (names.length >= 4) return "U"; //not a name
			if (_male == null && _female == null) return "U";
			for (String name : names) {
				if (_female.containsKey(name) && _male.containsKey(name)) {
					if (_female.get(name) > _male.get(name)*2) {
						return "F";
					}
					else if (_male.get(name) > _female.get(name)*2) {
						return "M";
					}
				}
				else if (_female.containsKey(name)) {
					return "F";
				}
				else if (_male.containsKey(name)) {
					return "M";
				}
			}
		}
		return "U";
	}
	
	public void labelGender(String file, String directory) {
		List<String> usernames = processing.GeneralUtils.readLines(file);
		int male = 0;
		int female = 0;
		
		for (String username : usernames) {
			
			Blog blog = Blog.processBlog(directory, directory, username);
				
			String gender = getGender(blog);
			if (gender.equals("F")) female++;
			else if (gender.equals("M")) male++;
		}
		System.out.println("Male: " + male + ", Female: " + female);
	}
}
