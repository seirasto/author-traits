package edu.columbia.demographics.gender;

import java.io.*;
import java.util.Arrays;
import java.util.Properties;

import processing.GeneralUtils;

import weka.core.*;
import weka.core.converters.ConverterUtils.DataSource;
import edu.columbia.demographics.DemographicDetector;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;

public class GenderDetector extends DemographicDetector {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		try {
			boolean overwrite = false;
			Properties prop = GeneralUtils.getProperties("config/config.properties");
			String xtract = prop.getProperty("xtract");
			String modelDirectory = prop.getProperty("model");
			String tagger = prop.getProperty("tagger");

			String trainArff = modelDirectory + "/gender.arff"; //"/gender/gender.arff-filtered.arff";
			String testArff = modelDirectory + "/gender.arff"; // "/gender/gender.arff-filtered.arff";
			
			String command = args[0];
			String iFile = args[1]; 
				    //e.g. "/proj/nlp/fluke/SCIL/corpora/wikipedia/WD_batch2_year3LJ/World_War_II__Archive_46___Obvious_omission.xml";
					//e.g. "/proj/nlp/fluke/SCIL/corpora/livejournal/agreement-batch1/100417.xml";
			String oFile = args[2];
					//e.g. "/proj/nlp/users/sara/influence/processed/processed-" + fName + "/" + fName;
					//e.g. "/proj/nlp/users/sara/influence/processed/processed-" + fName + "/" + fName + ".dep.pos";
			
			GenderDetector genderD = new GenderDetector(new MaxentTagger(tagger), LexicalizedParser.loadModel(), xtract);
			String cls = "svm";
			boolean binary = false;
			
			if (command.equals("-experiment")) {
				genderD.runExperiments(trainArff, testArff, "GENDER", cls);
			}
			else if (command.equals("-model")) {
	
				System.out.println("Loading/Creating: " + modelDirectory + "/" + cls + "-gender" + (binary ? "-binary" : "") + ".model");
				if (overwrite || !new File(modelDirectory + "/" + cls + "-gender" + (binary ? "-binary" : "") + ".model").exists()) 
					genderD.generateModel(trainArff,cls,modelDirectory + "/");
				genderD.loadClassifier(modelDirectory + "/" + cls + "-gender" + (binary ? "-binary" : "") + ".model",trainArff);
				genderD.classify(cls, trainArff,testArff, binary);
			}
			else if (command.equals("-run")) {
				if (!new File(modelDirectory + "/" + cls + "-gender" + (binary ? "-binary" : "") + ".model").exists()) {
					System.out.println("Please run model command. Model does not exist at " + modelDirectory + "/" + cls + "-gender" + (binary ? "-binary" : "") + ".model");
					System.exit(0);
				}	
				genderD.loadClassifier(modelDirectory + "/" + cls + "-gender" + (binary ? "-binary" : "") + ".model",trainArff);
				genderD.run(iFile, oFile,cls);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public GenderDetector(MaxentTagger tagger, LexicalizedParser parser, String xtract) {
		super(tagger, parser, "gender", new Integer [] {0,3,5,9,10,11,12,13,15,16,17,18,19,21}, xtract, false);
	}

	public void classify(String cls, String trainArff, String testArff, boolean binary) {
		Instances train = removeIrrelevantAttributes(loadInstances(trainArff));
		train.setClass(train.attribute("GENDER"));
		System.out.println("Class Breakdown Training: " + Arrays.toString(train.attributeStats(train.classIndex()).nominalCounts));
		Instances test = removeIrrelevantAttributes(loadInstances(testArff));
		test.setClass(test.attribute("GENDER"));
		System.out.println("Class Breakdown Testing: " + Arrays.toString(test.attributeStats(test.classIndex()).nominalCounts));
		this.classify(_classifier, cls, train, test,binary);
	}
	
	public void generateModel(String arff, String cls, String output) {
		try {
			Instances instances = DataSource.read(arff); //loadInstances(arffDir);
			System.out.println("There are " + instances.numInstances() + " instances.");
			
			// remove irrelevant attributes and instances
			instances = removeIrrelevantAttributes(instances);
			//instances = removeUnknown(instances);
			//instances = updateGender(instances);
			System.out.println("There are " + instances.numInstances() + " instances.");
			generateModel(instances,cls,output,"GENDER");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/*protected Instances updateGender(Instances instances) {
		
		instances.renameAttribute(instances.attribute("GENDER"),"GENDER3");	
		//Attribute gender3 = instances.attribute("GENDER3");	
		//Attribute gender = newInstances.attribute("GENDER");
		//newInstances.deleteAttributeAt(gender.index());
		Instances newInstances =  new Instances(instances,0);
		
		FastVector g = new FastVector();
		g.addElement("M");
		g.addElement("F");
		Attribute gender = new Attribute("GENDER",g);
		newInstances.insertAttributeAt(gender,newInstances.numAttributes()-1);
		instances.insertAttributeAt(gender,instances.numAttributes()-1);
		
		for (int i = 0; i < instances.numInstances(); i++) {
			Instance instance = instances.instance(i);
			instance.setValue(instances.attribute("GENDER"),instance.stringValue(instances.attribute("GENDER3")));
			newInstances.add(instance);
		}
		newInstances.deleteAttributeAt(newInstances.attribute("GENDER3").index());
		instances.setClass(instances.attribute("GENDER"));
		return instances;
	}
	
	protected Instances removeUnknown(Instances instances) {
		
		Attribute gender = instances.attribute("GENDER");
		for (int i = 0; i < instances.numInstances(); i++) {
			Instance instance = instances.instance(i);
			
			if (instance.stringValue(gender).equals("U")) {
				instances.delete(i);
				i--;
			}
		}
		return instances;
	}*/
	
	protected Instances removeIrrelevantAttributes(Instances instances) {
		removeIrrelevantDemographicAttributes(instances);
		
		/*String remove = "";
		
		for (int index = 0; index < instances.numAttributes(); index++) {
			if (instances.attribute(index).name().startsWith("POS ")) 
				remove += (index+1) + ",";
		}
		
		try {
			instances = InstancesProcessor.removeList(instances,remove);
		//instances.deleteAttributeAt(instances.attribute("YEAR").index());
		} catch(Exception e) {
			e.printStackTrace();
		}*/
		
		//instances.deleteAttributeAt(instances.attribute("YEAR").index());
		return instances;
	}
	
	
	/*public void run(String iFile, String oFile, String cls) {
		
		try {
			// load training instances
			//Instances train = DataSource.read(arff); //loadInstances(arff);
			//_train = removeIrrelevantAttributes(train);
			//_train = removeUnknown(train);
			//_train = updateGender(train);
			
			run(iFile, oFile, cls);
			} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

	}*/
}
