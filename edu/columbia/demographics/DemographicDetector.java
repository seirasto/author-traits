package edu.columbia.demographics;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.*;
//import weka.classifiers.evaluation.NominalPrediction;
import weka.classifiers.functions.*;
import weka.classifiers.misc.SerializedClassifier;
import weka.classifiers.trees.*;
import weka.core.*;
import weka.core.converters.ConverterUtils.DataSource;

import java.io.*;
import java.util.*;

import edu.columbia.utilities.liwc.LIWC;
import edu.columbia.utilities.weka.InstancesProcessor;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;

import processing.GeneralUtils;
import processing.SwapEmoticons;
import social_media.Features;

import blog.threaded.livejournal.Blog;

public abstract class DemographicDetector {

	HashSet<String> _dictionary;
	HashSet<String> _emoticons;
	HashSet<String> _acronyms;
	MaxentTagger _tagger;
	String _xtract;
	FastWeka _weka;
	SwapEmoticons _swapper;
	LexicalizedParser _parser;
	protected SerializedClassifier _classifier;
	String _type;
	protected Instances _train;
	Features _smFeatures = null;
	boolean _twitter = false;
	LIWC _liwc = null;
	
	public DemographicDetector(String type, Integer [] attributes, String xtract, boolean twitter) {
		this(null,null,type, attributes, xtract, twitter);
		try {
			Properties prop = GeneralUtils.getProperties("config/config.properties");
			String tagger = prop.getProperty("tagger");
			_tagger = new MaxentTagger(tagger);

		} catch(Exception e) {
			e.printStackTrace();
		}
	}
		
	public DemographicDetector(MaxentTagger tagger, LexicalizedParser parser, String type, Integer [] attributes, String xtract, boolean twitter) {
		try {
			Properties prop = GeneralUtils.getProperties("config/config.properties");
			String emoticonFile = prop.getProperty("emoticons_directory") + "/emoticons.txt.codes";
			String acronymFile = prop.getProperty("emoticons_directory") + "/acronyms.txt";
			String dictionaryFile = prop.getProperty("dictionary");
			_smFeatures = new Features(dictionaryFile, prop.getProperty("emoticons_directory") + "/emoticon_sub.txt", acronymFile);
			//String xtract = prop.getProperty("xtract");
			
			if (new File(prop.getProperty("liwc")).exists())
				_liwc = new LIWC(prop.getProperty("liwc"));
			
			
			_emoticons = new HashSet<String>(processing.SwapEmoticons.readCodes(new File(emoticonFile), false).values());
			_acronyms = FastWeka.loadDataFile(acronymFile);
			_dictionary  = FastWeka.loadDataFile(dictionaryFile);
			_tagger = tagger;
			_xtract = xtract;
	    	_type = type;
	    	
	    	if (type.equals("age")) type = "year"; 
	    	
			// 2. generate instances
			_weka = new FastWeka(null, null, null, null, type, "1982",null, null,_twitter ? "twitter" : "lj");
			
			//Integer [] a = {0,3,4,5,9,10,11,12,13,15,16,17,18,19,21,22};
			_weka.loadAttributes(null, Arrays.asList(attributes), _xtract + type + "/", 1500, "ef", true);
			
			_swapper = new processing.SwapEmoticons();
	    	_parser = parser;
	    	_twitter = twitter;
		} catch (Exception e) {
			System.err.println("[FastWeka.run] " + e);
   			e.printStackTrace();
		}
	}
	
	private Instances subset(Instances instances, String [] experiments, String type, boolean exact) {
		
		Instances subset = new Instances(instances);
		String remove = "";
		
		try {
			
			for (int index = 0; index < subset.numAttributes(); index++) {
				if (subset.attribute(index).name().equals(type)) continue; 
				
				boolean found = false;
				
				for (String experiment : experiments) {
					if (experiment.isEmpty()) continue;
					if (subset.attribute(index).name().startsWith((String)experiment)) {
					    //System.err.print(subset.attribute(index) + ": " + eval.evaluateAttribute(index) + ", ");
						found = true;
						break; 
					}
				}
				if (!found) {
					//subset.deleteAttributeAt(index);
					//index--;
					remove += (index+1) + ",";
				}
				/*else {
					System.out.print(subset.attribute(index).name() + ", ");
				}*/
			}
			subset = InstancesProcessor.removeList(subset, remove);
		} catch (Exception e) {
			e.printStackTrace();
		}
		//System.out.println("");
		return subset;
	}
	
	public Instances yearToAge(Instances instances) {
   		ArrayList<String> classes = new ArrayList<String>();
		classes.add("<1981");
		classes.add(">1981");
		Attribute a = new Attribute("AGE", classes);
		instances.insertAttributeAt(a, instances.numAttributes()-1);
		
		
		for (int i = 0; i < instances.numInstances(); i++) {
			double value = instances.instance(i).value(instances.attribute("YEAR"));
			if (value >= 1981) instances.instance(i).setValue(instances.attribute("AGE"), 1);
			else instances.instance(i).setValue(instances.attribute("AGE"), 0);
		}
		instances.deleteAttributeAt(instances.attribute("YEAR").index());
		instances.setClass(instances.attribute("AGE"));
		return instances;
	}
	
	public void runExperiments(String fullTrainArff, String fullTestArff, String classAttribute, String cls) {
		
		try {
			Instances fullTrain = DataSource.read(fullTrainArff);
			Instances fullTest = DataSource.read(fullTestArff);
			fullTrain.setClass(fullTrain.attribute(classAttribute));
			fullTest.setClass(fullTest.attribute(classAttribute));

			
			if (classAttribute.equals("AGE")) {
				fullTrain = yearToAge(fullTrain);
				fullTest = yearToAge(fullTest);
			}
			
			int [] stats = fullTrain.attributeStats(fullTrain.classIndex()).nominalCounts;
			int majority = 0;
			int max = stats[0];
			for (int i = 1; i < stats.length; i++) {
				if (stats[i] > max) {
					max = stats[i];
					majority = i;
				}
			}
			System.out.print("majority");
			for (int index = 0; index < fullTest.numInstances(); index++) {
				if (fullTest.instance(index).classValue() == majority) System.out.print(",1");
				else System.out.print(",0");
			}
			System.out.println();
		
			// SM, LIWC, POS, BIGRAM, SYNTAX_BIGRAM, GRAM, POS_BIGRAM, POS_SYNTAX_BIGRAM, MODE
			// experiments
			ArrayList<String []> experiments = new ArrayList<String []>();
			/*experiments.add(new String [] {"BIGRAM","GRAM"}); //n-grams
			experiments.add(new String [] {"SM"});
			experiments.add(new String [] {"LIWC"});
			experiments.add(new String [] {"POS ", "POS_BIGRAM "});
			experiments.add(new String [] {"POS_SYNTAX_BIGRAM","SYNTAX_BIGRAM"});
			experiments.add(new String [] {"POS", "SYNTAX", "BIGRAM","GRAM","LIWC","SM"});
			experiments.add(new String [] {"POS", "SYNTAX", "BIGRAM","GRAM","LIWC","SM","MODE"});*/
			experiments.add(new String [] {"POS", "SYNTAX", "BIGRAM","GRAM","LIWC"});
			experiments.add(new String [] {"POS", "SYNTAX", "BIGRAM","GRAM"});
			experiments.add(new String [] {"SYNTAX", "BIGRAM","GRAM"});
			
			ArrayList<double []> results = new ArrayList<double []>();
			
			for (String [] experiment : experiments) {
				System.out.print(Arrays.toString(experiment) + " ");
				
				Instances train = subset(fullTrain, experiment, classAttribute, false);
				Instances test = subset(fullTest, experiment, classAttribute, false);
				
				Classifier classifier = getClassifier(cls);
				
				train.setClass(train.attribute(classAttribute));
				test.setClass(test.attribute(classAttribute));
				
				/*int [] stats = train.attributeStats(train.classIndex()).nominalCounts;
				System.out.println("Class Breakdown Training: " + Arrays.toString(stats));
				stats = test.attributeStats(test.classIndex()).nominalCounts;
				System.out.println("Class Breakdown Testing: " + Arrays.toString(stats));*/

				
				classifier.buildClassifier(train);
				results.add(classify(classifier, cls, train, test));
			
			}
		
			
			for (int index = 0; index < results.size(); index++) {
				System.out.println(Arrays.toString(experiments.get(index)).replaceAll(", ", "-").replaceAll(" ", "") + "," + Arrays.toString(results.get(index)).replace("[", "").replace("]", "").replace(".0","").replaceAll("\\s+",""));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public void generateModel(Instances instances, String cls, String output, String classAttribute) {
		
		try {
			System.out.println("[" + new Date().toString() + "] Generating Model: " + cls + "-" + classAttribute.toLowerCase() + ".model ...");
			Classifier classifier = getClassifier(cls);
			
			instances.setClass(instances.attribute(classAttribute));
			
			int [] stats = instances.attributeStats(instances.classIndex()).nominalCounts;
			System.out.println("Class Breakdown Training: " + Arrays.toString(stats));
			
			classifier.buildClassifier(instances);
			Debug.saveToFile(output + "/" + cls + "-" + classAttribute.toLowerCase() + ".model", classifier);
			System.out.println("DONE");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Instances loadInstances(String arffDir) {
		Instances instances = null;

		try {
			File [] arffFiles = null;
			if (new File(arffDir).isDirectory())
			 arffFiles = new File(arffDir).listFiles();
			else
			 arffFiles = new File [] {new File(arffDir)};
			
			for (File arff : arffFiles) {
				if (!arff.toString().endsWith(".arff")) continue;
				Instances second = DataSource.read(arff.toString());
				if (instances == null) instances = second;
				if (!new File(arffDir).isDirectory()) return instances;
				
				for (int i = 0; i < second.numInstances(); i++) 
					instances.add(second.instance(i));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return instances;
	}
	
	protected Instances removeIrrelevantDemographicAttributes(Instances instances) {
		if (instances.attribute("FRIENDS") != null) instances.deleteAttributeAt(instances.attribute("FRIENDS").index());
		if (instances.attribute("POSTS") != null) instances.deleteAttributeAt(instances.attribute("POSTS").index());
		if (instances.attribute("COMMENTS") != null) instances.deleteAttributeAt(instances.attribute("COMMENTS").index());
		if (instances.attribute("ENTRIES") != null) instances.deleteAttributeAt(instances.attribute("ENTRIES").index());
		
		/*for (int i = 0; i < instances.numAttributes(); i++) {
			if (instances.attribute(i).name().trim().startsWith("INTEREST")) {
				instances.deleteAttributeAt(i);
				i--;
			}
		}*/
		
		return instances;
	}
	
	protected abstract Instances removeIrrelevantAttributes(Instances instances);

	public void loadClassifier(String model, String arff) {
		try {
			// load training instances
			_train = DataSource.read(arff); //loadInstances(arff);
			_train = removeIrrelevantAttributes(_train);
						
			// 1. load classifier
			_classifier = new SerializedClassifier();
			_classifier.setModelFile(new File(model));
			//if (cls.equals("linear")) System.out.println(classifier);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void run(String iFile, String oFile, String cls) {
		try {
			// 3. classify 
			HashMap<String,Double> results = classify(_classifier,cls, _train, iFile, oFile, _weka, _swapper, _tagger, _parser);
			// 4. output instances - as list with users	
			System.out.println(results.toString());
			printResults(results, oFile + "." + _type);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public double [] classify(Classifier classifier, String cls, Instances train, Instances test) {
		return classify(classifier, cls, train, test, false, 0);
	}
	
	public double [] classify(Classifier classifier, String cls, Instances train, Instances test, boolean binary) {
		return classify(classifier, cls, train, test, binary, 0);
	}
	
	public double [] classify(Classifier classifier, String cls, Instances train, Instances test, boolean binary, double median) {
		try {
			
			Evaluation evaluation = new Evaluation(train);
			/*System.out.println("==== CROSSVALIDATION ====");
			evaluation.crossValidateModel(classifier, train, 10, new Random(1));
			if (!cls.equals("linear")) System.out.println(evaluation.toClassDetailsString());
			System.out.println(evaluation.toSummaryString());*/
			
			/*try {
				System.out.println("==== CROSSVALIDATION ====");
				train.setClassIndex(train.numAttributes()-1);
				evaluation.crossValidateModel(classifier, train, 10, new Random(1));
				if (!cls.equals("linear")) System.out.println(evaluation.toClassDetailsString());
				System.out.println(evaluation.toSummaryString());
				//evaluation.evaluateModel(classifier, test);
				//System.out.println(evaluation.toSummaryString());
				//if (!cls.equals("linear")) System.out.println(evaluation.toClassDetailsString());
			} catch (Exception e) {
				e.printStackTrace();
			}*/
			
			if (!train.equalHeaders(test)) 
				  throw new IllegalStateException("Incompatible train and test set!"); 
			//System.err.println("classifying...");
			//train.setClassIndex(train.numAttributes()-1);		
			//test.setClassIndex(test.numAttributes()-1);
			
			if (binary) {
				System.err.println("Convert to binary...");
				train = InstancesProcessor.toBinary(train);
				test = InstancesProcessor.toBinary(test);
			}
			
			if (!cls.equals("linear")) { 
				int [] stats = train.attributeStats(train.classIndex()).nominalCounts;
				System.out.println("Class Breakdown Training: " + Arrays.toString(stats));
				stats = test.attributeStats(test.classIndex()).nominalCounts;
				System.out.println("Class Breakdown Testing: " + Arrays.toString(stats));
			}
			evaluation.evaluateModel(classifier, test);
			//System.out.println("==== TEST SET ====");
			if (!cls.equals("linear")) System.out.println((evaluation.correct()/evaluation.numInstances())*100); //evaluation.toClassDetailsString());
			//System.out.println(evaluation.toSummaryString());
			if (cls.equals("linear")) {
				
				double avg = 0;
				double medAvg = 0;
				for (int index = 0; index < test.numInstances(); index++) {
					double predicted = classifier.classifyInstance(test.instance(index));
					double actual = test.instance(index).classValue();
					System.out.println(actual + "," + predicted + "," + Math.abs(actual - predicted) + "," + Math.abs(median - actual));
					avg += Math.abs(actual - predicted);
					medAvg += Math.abs(median - actual);
				}
				System.out.println(avg / test.numInstances() + ", " + medAvg / test.numInstances());
				/*FastVector predictions = evaluation.predictions();
				
				for (int i = 0; i < predictions.size(); i++) {
					NominalPrediction prediction = (NominalPrediction)predictions.elementAt(i);
					System.out.println(prediction.actual() + "," + prediction.predicted() + "," + Math.abs(prediction.actual() - prediction.predicted()));
				}*/
			}
			
			double [] results = new double[test.numInstances()];
			for (int index = 0; index < test.numInstances(); index++) {
				results[index] = (classifier.classifyInstance(test.instance(index)) == test.instance(index).classValue() ? 1 : 0);
			}
			return results;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public HashMap<String,Double> classify(SerializedClassifier classifier, String cls, Instances train, String iFile, String oFile, FastWeka weka, SwapEmoticons swapper, MaxentTagger tagger, LexicalizedParser parser) {
		// process file
		if (!new File(oFile + ".dep.pos").exists()) 
			Blog.addSyntaxDependencies(iFile, oFile + ".dep.pos", tagger, parser, true, swapper);
		
		Blog blog = Blog.processBlog(oFile + ".dep.pos");
		
		HashSet<String> usernames = getUsernames(blog);
		HashMap<String,Double> results = new HashMap<String,Double>();
		Instances test = new Instances(train, 0);
		test.setClassIndex(test.numAttributes()-1);		
		for (String username : usernames) {
			try {
				test.add(getInstance(train, blog, username, weka));
				double value = classifier.classifyInstance(test.lastInstance());
				results.put(username,value);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		return results;
	}
	
	/**
	 * Print results to a file
	 * @param results
	 * @param outputFile
	 */
	public void printResults(HashMap<String,Double> results, String outputFile) {
		
		try {
			PrintWriter output = new PrintWriter(outputFile);
			
			for (String username : results.keySet()) {
				output.println(username + "\t" + results.get(username));
			}
			output.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private HashSet<String> getUsernames(Blog blog) {
		
		HashSet<String> usernames = new HashSet<String>();
		
		usernames.add(blog.getUsername());
		
		Iterator<String> it = blog.getEntries();
		
		while (it.hasNext()) {
			
			// first entry
			blog.threaded.livejournal.Entry et = (blog.threaded.livejournal.Entry)blog.getEntry((String)it.next());
			
			// then comments
			Set<String> comments = et.getCommentKeys();
	
			for (String id : comments) {
				usernames.add(et.getComment(id).getUsername());
			}
		}
		return usernames;
	}
	
	public Instance getInstance(Instances train, Blog blog, String username, FastWeka weka) {
		
		Instance instance;
		instance = new DenseInstance(train.numAttributes());
		
		String text = getText(blog, username, false);
		String pos = getText(blog, username, true);
		Hashtable<String,Double> attributeCount = FastWeka.computeSMFrequencyStats(blog, _liwc, _smFeatures, _twitter, _twitter ? 100 : 25); 
		//_smFeatures.compute(FastWeka.convertLinks(text, _twitter).split("\\s+")); //computeTermFrequencyStats(text);
		//attributeCount = addTimeStats(blog, username,attributeCount);
		//ArrayList<String> dependencies = getDependencies(blog,username);
		//text = text.replaceAll("/[A-Z\\p{Punct}]+ ", " ").replaceAll("\\p{Punct}", "").toLowerCase();
		
		for (int index = 0; index < train.numAttributes(); index++) {
				
			Attribute attribute = (Attribute)train.attribute(index);
			String name = attribute.name();
			
			if (name.startsWith("YEAR") || name.startsWith("GENDER") || name.startsWith("POLITICAL") || name.startsWith("RELIGION") || name.startsWith("AGE")) {
				/*int year = Integer.valueOf(new java.text.SimpleDateFormat("yyyy").format(blog.getDOB()));
				int lbd = Integer.valueOf(new java.text.SimpleDateFormat("yyyy").format(blog.getLastBuildDate()));
				instance.setValue(attribute, this.getRange(Integer.valueOf(
					rangeType.equals("year") ? year : lbd - year)));*/
				instance.setValue(attribute, 0);
			}			
			else if (name.startsWith("BIGRAM")) {
				instance.setValue(attribute, _weka.searchEntries(text,
						getQuery(name.substring("BIGRAM ".length()),weka),true)/attributeCount.get(FastWeka.NUM_WORDS));
			}
			else if (name.startsWith("SYNTAX_BIGRAM")) {
				instance.setValue(attribute, _weka.searchEntries(text,
						getQuery(name.substring("SYNTAX_BIGRAM ".length()),weka),true)/attributeCount.get(FastWeka.NUM_WORDS));
				//instance.setValue(attribute, searchDependencies(dependencies,name.substring("SYNTAX_BIGRAM ".length()),false));
			}
			else if (name.startsWith("POS_BIGRAM")) {
				instance.setValue(attribute, _weka.searchEntries(pos,
						getQuery(name.substring("POS_BIGRAM ".length()),weka),true)/attributeCount.get(FastWeka.NUM_WORDS));
			}
			else if (name.startsWith("POS_SYNTAX_BIGRAM")) {
				instance.setValue(attribute, _weka.searchEntries(pos,
						getQuery(name.substring("POS_SYNTAX_BIGRAM ".length()),weka),true)/attributeCount.get(FastWeka.NUM_WORDS));
				//instance.setValue(attribute, searchDependencies(dependencies,name.substring("POS_SYNTAX_BIGRAM ".length()),true));
			}
			else if (name.startsWith("GRAM")) {
				instance.setValue(attribute, _weka.searchEntries(text, name.substring("GRAM ".length()),false)/attributeCount.get(FastWeka.NUM_WORDS));
			}
			else if (name.startsWith("POS")) {
				instance.setValue(attribute, _weka.searchEntries(pos, name.substring("POS".length()),false)/attributeCount.get(FastWeka.NUM_WORDS));
			}
			// one of the attributes that we computed counts for (eg ACRONYM, COMMENTS, etc..)
			else if (attributeCount.containsKey(name)) {
				instance.setValue(attribute, attributeCount.get(name) / attributeCount.get(FastWeka.NUM_WORDS));
			}
			else {
				System.err.println("[DemographicDetector.getInstance] invalid attribute: " + name);
			}
		}
		return instance;
	}
	
	private String getQuery(String key, FastWeka weka) {
		if (weka.bigrams.containsKey(key)) return weka.bigrams.get(key);
		String [] words = key.split("\\s+");
		if (words.length == 2) {
			if (weka.bigrams.containsKey(words[1] + " " + words[0])) return weka.bigrams.get(words[1] + " " + words[0]);
			return null;
		}
		return null;
	}
	
	/*private int search(String text, String query) {
		int count = 0;
		try {
			
			Pattern p = Pattern.compile("\\b(" + query + ")\\b",Pattern.CASE_INSENSITIVE);
			Matcher m = p.matcher(text);
			
			while (m.find()) count++;

		} catch (Exception e) {
			System.err.println("[FastWeka.searchEntries] Error occurred in finding match for \"" 
					+ query + "\" in entry " + text + "\n: " + e);
			e.printStackTrace();
		}
		return count;
	}*/
	
	/*private int searchDependencies(ArrayList<String> dependencies, String query, boolean pos) {
		int count = 0;
		
		//System.out.println(query);
		String [] words = query.split(" ");
		
		for (String dependency : dependencies) {
			//String type = dependency.substring(0,dependency.indexOf("(")).trim();
			String sub =  dependency.substring(dependency.indexOf("(")+1,dependency.indexOf("-")).trim().toLowerCase();
			String obj = dependency.substring(dependency.indexOf(",")+2,dependency.lastIndexOf("-")).trim().toLowerCase();

			String [] posWords = {"",""};
			if (pos) {
				posWords[0] = _tagger.tagString(sub);
				posWords[1] = _tagger.tagString(obj);
				posWords[0] = posWords[0].substring(posWords[0].indexOf("/")+1).trim().toLowerCase();
				posWords[1] = posWords[1].substring(posWords[1].indexOf("/")+1).trim().toLowerCase();
			}

			
			if (!pos && sub.equalsIgnoreCase(words[0].trim()) && obj.equalsIgnoreCase(words[1].trim())) {
				count++;
			}
			else if (!pos && sub.equalsIgnoreCase(words[1].trim()) && obj.equalsIgnoreCase(words[0].trim())) {
				count++;
			}
			else if (pos && sub.equalsIgnoreCase(words[0].trim()) && posWords[1].equalsIgnoreCase(words[1].trim())) {
				count++;
			}
			else if (pos && sub.equalsIgnoreCase(words[1].trim()) && posWords[1].equalsIgnoreCase(words[0].trim())) {
				count++;
			}
			else if (pos && obj.equalsIgnoreCase(words[0].trim()) && posWords[0].equalsIgnoreCase(words[1].trim())) {
				count++;
			}
			else if (pos && obj.equalsIgnoreCase(words[1].trim()) && posWords[0].equalsIgnoreCase(words[0].trim())) {
				count++;
			}
		}
		return count;
	}*/
	
	/*private ArrayList<String> getDependencies(Blog blog, String username) {
		Iterator<String> it = blog.getEntries();
		
		ArrayList<String> dependencies = new ArrayList<String>();
		
		while (it.hasNext()) {
			
			// first entry
			blog.threaded.livejournal.Entry et = (blog.threaded.livejournal.Entry)blog.getEntry((String)it.next());
			
			if (blog.getUsername().equals(username)) {
				for (blog.Sentence sentence : et.getSentences()) {
					dependencies.addAll(sentence.getDependencies());
				}
			}
	
			// then comments
			Set<String> comments = et.getCommentKeys();
	
			for (String id : comments) {
				blog.Comment comment = et.getComment(id);

				if (!comment.getUsername().equals(username)) continue;
				
				for (blog.Sentence sentence : comment.getSentences()) {
					dependencies.addAll(sentence.getDependencies());
				}
			}
		}
		return dependencies;
	}*/
	
	private String getText(Blog blog, String username, boolean pos) {
		Iterator<String> it = blog.getEntries();
		
		String text = "";
		
		while (it.hasNext()) {
			
			// first entry
			blog.threaded.livejournal.Entry et = (blog.threaded.livejournal.Entry)blog.getEntry((String)it.next());
			
			if (blog.getUsername().equals(username)) {
				String entry = pos ? _weka.getTruncatedPOS(et.getEntry().toLowerCase().replaceAll("_", "/").toLowerCase()) : et.getEntryText().toLowerCase();
				text += entry + " ";
			}
	
			// then comments
			Set<String> comments = et.getCommentKeys();
	
			for (String id : comments) {
				blog.threaded.livejournal.Comment comment = (blog.threaded.livejournal.Comment)et.getComment(id);

				if (!comment.getUsername().equals(username)) continue;
				
				String entry = pos ? _weka.getTruncatedPOS(comment.getComment().toLowerCase().replaceAll("_", "/").toLowerCase()) : 
					comment.getCommentText().toLowerCase();
				text += entry + " ";	
			}
		}
		text = FastWeka.convertLinks(text, _twitter);
		return text;
	}
	
	/*public Hashtable<String,Double> addTimeStats(Blog blog, String username, Hashtable<String,Double> stats) {
		// most popular time & day of post
		int [] days = new int[7];
		int [] times = new int[24];
		
		Iterator<String> it = blog.getEntries();
		
		while (it.hasNext()) {
			
			// first entry
			blog.threaded.livejournal.Entry et = (blog.threaded.livejournal.Entry)blog.getEntry((String)it.next());
			
			if (blog.getUsername().equals(username)) {
				Calendar date = Calendar.getInstance();
				date.setTime(et.getDate());
					
				if (date.get(Calendar.DAY_OF_WEEK) > 0)
					days[date.get(Calendar.DAY_OF_WEEK)-1]++;
				if (date.get(Calendar.HOUR_OF_DAY) > 0) 
					times[date.get(Calendar.HOUR_OF_DAY)-1]++;
			}
	
			// then comments
			Set<String> comments = et.getCommentKeys();
	
			for (String id : comments) {
				
				blog.Comment comment = et.getComment(id);
				
				if (!comment.getUsername().equals(username) || comment.getDate() == null) continue;
				
				Calendar date = Calendar.getInstance();
				date.setTime(comment.getDate());
					
				if (date.get(Calendar.DAY_OF_WEEK) > 0)
					days[date.get(Calendar.DAY_OF_WEEK)-1]++;
				if (date.get(Calendar.HOUR_OF_DAY) > 0) 
					times[date.get(Calendar.HOUR_OF_DAY)-1]++;
			}
		}		
		//stats.put("AVG_TIME",this.getAverage(times));
		stats.put("MODE_TIME",(double)FastWeka.getMode(times));
		//stats.put("AVG_DAY",this.getAverage(days));
		stats.put("MODE_DAY",(double)FastWeka.getMode(days));		
		return stats;
	}*/
	
	/*public Hashtable<String,Double> computeTermFrequencyStats(String text) {
		
		Hashtable<String,Double> stats = new Hashtable<String,Double>();
		
		stats.put("PUNCTUATION",0.0);
		stats.put("CAPITALIZATIONS",0.0);
		stats.put("SENTENCE_LENGTH",0.0);
		stats.put("SLANG",0.0);
		stats.put("EMOTICONS",0.0);
		stats.put("ACRONYMS",0.0);
		stats.put("LINKS",0.0);		
		
		double misspellings = 0;
		double numEmoticons = 0;
		double numAcronyms = 0;
		double punctuation = 0;
		double numLinks = 0;
		double numCapitalWords = 0;
		double numWords = 0;
		double sentenceLength = 0;
		
		List<String> sentences = processing.StringProcessing.SentenceSplitter(text);
		
		for (String sentence : sentences) {
				try {
				
					String [] words = sentence.split("\\s+");
					numWords += words.length;			
					sentenceLength += sentence.length();
							
					for (int index = 0; index < words.length; index++) {
					
						String word = words[index];
						
						if (word.contains("LLLINKKK") || word.contains("IIIMAGEEE")) {
							numLinks++;
							if (word.replaceAll("(LLLINKKK|IIIMAGEEE)", "").equals("")) continue;
							word = word.replaceAll("(LLLINKKK|IIIMAGEEE)","");
						}
					
						// to capture normal punctuation such as: . and ,
						if (word.length() > 1 && !word.matches("\\p{Punct}+") 
								&& String.valueOf(word.charAt(word.length()-1)).matches("\\p{Punct}")) {
							word = word.replaceFirst("\\p{Punct}+$", "");
							punctuation++;
						}
					
						// at least two letters to avoid I,A
						if (word.matches("[A-Z][A-Z']+")) numCapitalWords++;
						
						if (_emoticons.contains(word)) {
							numEmoticons++;
							continue;
						}
						else if (word.toLowerCase().matches("\\p{Punct}+")) {
							punctuation++;
							continue;
						}
						else if (_dictionary.contains(word.replaceAll("\\p{Punct}","").toLowerCase())
								|| _dictionary.contains(word.toLowerCase())
								|| word.matches("[0-9]+")) continue;
						
						else if (_acronyms.contains(word.toLowerCase())) numAcronyms++;
						else misspellings++;
					}
				}
				catch (Exception e) {
					System.err.println("FastWeka.computeTermFrequencyStats] error on sentence: " + sentence + ": " + e);
					e.printStackTrace();
				}
			}

			// normalize counts over entry size (in term of words)
			stats.put("PUNCTUATION", stats.get("PUNCTUATION") + (punctuation / numWords));
			stats.put("CAPITALIZATIONS", stats.get("CAPITALIZATIONS") + (numCapitalWords / numWords));
			stats.put("SLANG", stats.get("SLANG") + (misspellings / numWords));
			stats.put("EMOTICONS", stats.get("EMOTICONS") + (numEmoticons / numWords));
			stats.put("ACRONYMS", stats.get("ACRONYMS") + (numAcronyms / numWords));
			stats.put("LINKS", stats.get("LINKS") + (numLinks / numWords));	
			stats.put("SENTENCE_LENGTH", sentenceLength / (double)sentences.size());	

		return stats;
	}*/
	
	public  Classifier getClassifier(String classifier)  {
		if (classifier.equals("nb")) return new NaiveBayes();
		if (classifier.equals("j48")) return new J48();
		//if (classifier.equals("id3")) return new Id3();
		if (classifier.equals("svm")) return new SMO();
		if (classifier.equals("logistic")) return new Logistic();
		if (classifier.equals("linear")) {
			try {
				LinearRegression r = new LinearRegression();
				r.setEliminateColinearAttributes(false);
				String [] options = {"-S","1"};
				r.setOptions(options);
				System.out.println(r.getAttributeSelectionMethod());
				return r;
			} catch (Exception e) {
				e.printStackTrace();
			}
			//return new SMOreg();
		}
		return null;
	}
}
