/**
 * 
 */
package edu.columbia.demographics.age;

import weka.core.*;
import weka.core.converters.ConverterUtils.DataSource;
import java.io.File;
import java.util.ArrayList;
import java.util.Properties;

import processing.GeneralUtils;

import edu.columbia.demographics.*;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;

/**
 * @author sara
 *
 */
public class AgeDetector extends DemographicDetector {
	
	double _median = 0;
	boolean _age = false;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		try {
			boolean overwrite = false;
			Properties prop = GeneralUtils.getProperties("config/config.properties");
			String modelDirectory = prop.getProperty("model");
			String tagger = prop.getProperty("tagger");
			String xtract = prop.getProperty("xtract");
			String arff = modelDirectory + "year.arff"; //"/train/year/year.arff-filtered.arff";
			String testArff = modelDirectory + "year.arff"; // "/test/year/year.arff-filtered.arff";
			
			String command = args[0];
			String iFile = args[1]; 
					//e.g. "/proj/nlp/fluke/SCIL/corpora/wikipedia/WD_batch2_year3LJ/World_War_II__Archive_46___Obvious_omission.xml";
					//e.g. "/proj/nlp/fluke/SCIL/corpora/livejournal/agreement-batch1/100417.xml";
			String oFile = args[2]; 
					//e.g. "/proj/nlp/users/sara/influence/processed/processed-World_War_II__Archive_46___Obvious_omission.xml/World_War_II__Archive_46___Obvious_omission.xml";
					//e.g. "/proj/nlp/users/sara/influence/processed/processed-100417.xml/100417.xml.dep.pos";
			
			boolean age = true;
			
			AgeDetector ageD = new AgeDetector(new MaxentTagger(tagger),LexicalizedParser.loadModel(),xtract,age);
			String cls = "svm";
			
			if (command.equals("-experiment")) {
				ageD.runExperiments(arff, testArff, "AGE", cls);
			}
			else if (command.equals("-model")) {
				if (overwrite || !new File(modelDirectory + "/" + cls + "-" + (age ? "age" : "year") + ".model").exists()) 
					ageD.generateModel(arff,cls,modelDirectory + "/");
				ageD.loadClassifier(modelDirectory + "/" + cls + "-" + (age ? "age" : "year") + ".model", arff);
				ageD.classify(cls, arff,testArff);
			}
			else if (command.equals("-run")) {
				if (!new File(modelDirectory + "/" + cls + "-" + (age ? "age" : "year") + ".model").exists()) {
					System.out.println("Please run model command. Model does not exist at " + modelDirectory + "/" + cls + "-" + (age ? "age" : "year") + ".model");
					System.exit(0);
				}
				ageD.loadClassifier(modelDirectory + "/" + cls + "-" + (age ? "age" : "year") + ".model", arff);
				ageD.run(iFile, oFile,cls);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public AgeDetector(MaxentTagger tagger, LexicalizedParser parser, String xtract, boolean age) {
		super(tagger,parser,age ? "age" : "year",new Integer[] {0,3,5,9,10,11,12,13,15,16,17,18,19,21}, xtract,false);
		_age = age;
	}
	
	public void classify(String cls, String trainArff, String testArff) {
		Instances train = removeIrrelevantAttributes(loadInstances(trainArff));
		//Attribute a = null;
		
		/*if (_age) {
	   		FastVector classes = new FastVector();
			classes.addElement("<1981");
			classes.addElement(">1981");
			a = new Attribute("AGE", classes);
			train.insertAttributeAt(a, train.numAttributes()-1);
			
			
			for (int i = 0; i < train.numInstances(); i++) {
				double value = train.instance(i).value(train.attribute("YEAR"));
				if (value > 1981) train.instance(i).setValue(train.attribute("AGE"), 1);
				else train.instance(i).setValue(train.attribute("AGE"), 0);
			}
			train.deleteAttributeAt(train.attribute("YEAR").index());
			train.setClass(train.attribute("AGE"));
		}
		else {
			train.setClass(train.attribute("YEAR"));
		}
		train = removeNaN(train);*/
		
		long avgYear = 0;
		
		for (int index = 0; index < train.numInstances(); index++) {
			double year = train.instance(index).classValue();
			avgYear += year;
		}
		double median = avgYear/(double)train.numInstances();
		System.out.println("There are " + train.numInstances() + " train instances. The median year is: " + median);
		
		Instances test = removeIrrelevantAttributes(loadInstances(testArff));
		if (test != null) {
			/*if (_age) {
				test.insertAttributeAt(a, test.numAttributes()-1);
				for (int i = 0; test != null && i < test.numInstances(); i++) {
					double value = test.instance(i).value(test.attribute("YEAR"));
					if (value > 1981) test.instance(i).setValue(test.attribute("AGE"), 1);
					else test.instance(i).setValue(test.attribute("AGE"), 0);
				}
				test.deleteAttributeAt(test.attribute("YEAR").index());
				test.setClass(test.attribute("AGE"));
			}
			else {
				test.setClass(test.attribute("YEAR"));
			}
			test = removeNaN(test);*/
			System.out.println("There are " + test.numInstances() + " test instances.");
		}
		this.classify(_classifier, cls, train, test, false, median);
	}
	
	private Instances removeNaN(Instances instances) {
		try {
			for (int index = 0; index < instances.numInstances(); index++) {
				double year = instances.instance(index).classValue();
				if (Double.isNaN(year)) {
					instances.delete(index);
					index--;
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return instances;
	}

	public void generateModel(String arff, String cls, String output) {
		try {
			Instances instances = DataSource.read(arff); //loadInstances(arffDir);
			
			// remove irrelevant instances
			instances = removeIrrelevantAttributes(instances);
			
			/*if (_age) {
		   		FastVector classes = new FastVector();
				classes.addElement("<1981");
				classes.addElement(">1981");
				Attribute a = new Attribute("AGE", classes);
				instances.insertAttributeAt(a, instances.numAttributes()-1);
				
				
				for (int i = 0; i < instances.numInstances(); i++) {
					double value = instances.instance(i).value(instances.attribute("YEAR"));
					if (value >= 1981) instances.instance(i).setValue(instances.attribute("AGE"), 1);
					else instances.instance(i).setValue(instances.attribute("AGE"), 0);
				}
				instances.deleteAttributeAt(instances.attribute("YEAR").index());
				instances.setClass(instances.attribute("AGE"));
			}
			else {
				instances.setClass(instances.attribute("YEAR"));
			}
			instances = removeNaN(instances);*/
			System.out.print(instances.numInstances() + " instances. ");
			
			// resample because too large
			/*Resample filter = new Resample();
			filter.setSampleSizePercent(20);
			filter.setInputFormat(instances);
			instances = Filter.useFilter(instances, filter);
			System.out.println("Reduced to " + instances.numInstances());*/
	
			generateModel(instances,cls,output,_age ? "AGE" : "YEAR");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	protected Instances removeIrrelevantAttributes(Instances instances) {
		if (instances == null) return null;
		removeIrrelevantDemographicAttributes(instances);
		
		if (_age) {
	   		ArrayList<String> classes = new ArrayList<String>();
			classes.add("<1981");
			classes.add(">1981");
			Attribute a = new Attribute("AGE", classes);
			instances.insertAttributeAt(a, instances.numAttributes()-1);
			
			
			for (int i = 0; i < instances.numInstances(); i++) {
				double value = instances.instance(i).value(instances.attribute("YEAR"));
				if (value > 1981) instances.instance(i).setValue(instances.attribute("AGE"), 1);
				else instances.instance(i).setValue(instances.attribute("AGE"), 0);
			}
			instances.deleteAttributeAt(instances.attribute("YEAR").index());
			instances.setClass(instances.attribute("AGE"));
		}
		else {
			instances.setClass(instances.attribute("YEAR"));
		}
		instances = removeNaN(instances);
		
		//instances.deleteAttributeAt(instances.attribute("GENDER").index());
		
		return instances;
	}
	
	public void run(String iFile, String oFile, String cls, String arff) {

		try {
			
			run(iFile, oFile, cls);
			} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
}
