Author Trait Detection:

This README includes instructions for generating models from the pre-existing lists and files.
It also includes instructions for running the existing models (without retraining) on a new file. 
The file must be in the LiveJournal XML format (see the example in input directory)

-------------------------

Data:

The data must be downloaded seperately from here:

http://www.cs.columbia.edu/~sara/data.php
Schler et al Bloggership Corpus: http://u.cs.biu.ac.il/~koppel/BlogCorpus.htm (available upon request in livejournal format)

-------------------------

Contents:

edu: code
config: configuration file
lists: list of authors per trait
models: models for predicting the author traits
input: example file
jars: required internal jars

There are also perl scripts for running the system as described below
 
-------------------------

To generate models from scratch:

1. Download the following jars and place in jars directory:
   commons-lang-2.6.jar  
   stanford-corenlp-1.3.5.jar
   xercesImpl.jar
   chiSquaredAttributeEval.jar  
   liblinear-1.94.jar    
   stanford-corenlp-1.3.5-models.jar  
   weka.jar
2. Download LIWC 2007 at http://www.liwc.net/ (It must be purchased, but it is optional. 
 -- If not used, remove "-liwc" option from runFastWeka.pl)
3. Set up configuration file to point to appropriate directories
   a. point dictionary to unix words file
4. run xtract: runXtract.pl
    Processing.java
    a. add POS and dependencies to files
    b. run xtract on each author trait
    ex: perl runXtract.pl gender 100 output
5. generate arff file: runFastWeka.pl 
   FastWeka.java
   a. "run" action (can be broken up to be generated faster)
   ex: perl runFastWeka.pl politics twitter 0 100 output
   b. if broken up, files must be concatenated
   c. "filter" action to filter n-gram features
   ex: perl runFastWeka.pl politics train.arff test.arff
6. generate model/make predictions: runModel.pl
   a. age: age/AgeDetector.java
   b. gender: gender/GenderDetector.java
   c. religion: religion/ReligionDetector.java
   d. politics: politics/PoliticsDetector.java

   ex: perl runModel.pl politics run input/World_War_II__Archive_46___Obvious_omission.xml output/World_War_II__Archive_46___Obvious_omission.xml

-------------------------

To use pre-existing models:

The input file must be in the LiveJournal XML format. 

1) If only plain text is provided:
 
The text may be placed in the input/empty.xml file. The two locations must be updated:
title: the name of the person being analyzed for author traits
description: The text written by the person
The creation of this file can be automated if it needs to be done multiple times.

2) If it is a discussion with multiple users (threaded or flat):

Please follow the format provided in the example file: input/World_War_II__Archive_46___Obvious_omission.xml

runModel.pl $type run $inputFile $outputFile
$type =
   a. age or year
   b. gender
   c. religion
   d. politics

   ex: perl runModel.pl politics run input/World_War_II__Archive_46___Obvious_omission.xml output/World_War_II__Archive_46___Obvious_omission.xml

-------------------------

References:

"Age Prediction in Blogs: A Study of Style, Content, and Online Behavior in Pre- and Post-Social Media Generations" 
Sara Rosenthal, Kathleen R. McKeown 
In proceedings of ACL-HLT 2011 

"Retrieving collocations from text: Xtract"
Frank Smadja
Computational Linguistics, 19:143-177.
